package harvest;

import harvest.controller.MongoDBController;
import harvest.model.*;
import harvest.paymentAPI.Transfers;
import javafx.scene.control.Alert;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class Utility {

    public static void showAlert(Alert.AlertType type, String s) {
        Alert alert = new Alert(type);
        alert.setContentText(s);
        alert.show();
    }

    public static Permissions SetPermissions(Role role) {
        Permissions permissions = new Permissions();
        switch (role) {
            case Admin:
                permissions.setAdminPermissions();
                break;
            case Reviewer:
                permissions.setReviewerPermissions();
                break;
            case Harvester:
                permissions.setHarvesterPermissions();
                break;
        }
        return permissions;
    }

    public static ArrayList<User> getUserArray(List<Document> users) {
        ArrayList<User> allUsers = new ArrayList<>();
        for (Document user : users) {
            JSONObject currUser = new JSONObject(user.toJson());
            User user1 = new User(currUser.getString("_id"), currUser.getString("firstname"),
                        currUser.getString("lastname"), currUser.getString("username"),
                        Role.valueOf(currUser.getString("role")), PaymentMethod.valueOf(currUser.getString("paymentmethod")));
            if (!currUser.getString("phonenumber").equals("")) user1.setPhoneNumber(currUser.getString("phonenumber"));
            if (!currUser.getString("country").equals("")) user1.setPhoneNumber(currUser.getString("country"));
            if (!currUser.getString("email").equals("")) user1.setPhoneNumber(currUser.getString("email"));
            if (currUser.has("accountno")) user1.setAccountNumber(currUser.getString("accountno"));
            if (currUser.has("bankname")) user1.setAccountNumber(currUser.getString("bankname"));
            if (currUser.has("bankcode")) user1.setAccountNumber(currUser.getString("bankcode"));
            if (currUser.has("bankcountry")) user1.setAccountNumber(currUser.getString("bankcountry"));
            allUsers.add(user1);
        }
        return allUsers;
    }

    public static ArrayList<Task> getTaskArray(List<Document> tasks) {
        ArrayList<Task> allTasks = new ArrayList<>();
        for (Document task : tasks) {
            JSONObject currTask = new JSONObject(task.toJson());
            Task task1 = new Task(currTask.getString("_id"), currTask.getString("title"),
                    currTask.getString("subtitle"), currTask.getInt("questionno"),
                    currTask.getDouble("costperquestion"), Currency.getInstance(currTask.getString("currency")),
                    TaskStatus.valueOf(currTask.getString("status")));
            ArrayList<Files> fileList = new ArrayList<>();
            task1.setDetails(currTask.getString("details"));
            task1.setReviewer(currTask.getString("reviewer"));
            task1.setHarvester(currTask.getString("harvester"));
            Map<String, List<String>> objectIdMap = new HashMap<>();
            if (!currTask.has("questionids") || currTask.get("questionids").toString().equals("{}"))
                task1.setQuestionIDs(new HashMap<>());
            else {
                JSONObject questionIdObject = currTask.getJSONObject("questionids");
                List<String> arrayList = new ArrayList<>();
                JSONArray questionIdArray = questionIdObject.getJSONArray("Questions");
                for (Object questionId : questionIdArray) {
                    arrayList.add(questionId.toString());
                }
                objectIdMap.put("Questions", arrayList);
                task1.setQuestionIDs(objectIdMap);
            }
            List<Question> questionArrayList = new ArrayList<>();
            if (currTask.get("questions").toString().equals("")) task1.setQuestions(new ArrayList<>());
            else {
                JSONArray questionArray = currTask.getJSONArray("questions");
                for (Object quest : questionArray) {
                    JSONObject question = new JSONObject(quest.toString());
                    Question question1 = new Question(question.getString("questionId"));
                    if (question.has("question")) question1.setQuestion(question.getString("question"));
                    if (question.has("instructions")) question1.setInstructions(question.getString("instructions"));
                    if (question.has("createdOn")) question1.setCreatedOn(question.optLong("createdOn"));
                    if (question.has("pathToImage")) question1.setPathToImage(question.getString("pathToImage"));
                    if (question.has("status"))
                        question1.setStatus(QuestionStatus.valueOf(question.getString("status")));
                    if (question.has("changeRequest"))
                        question1.setChangeRequest(question.getString("changeRequest"));
                    if (question.has("imageDescription"))
                        question1.setImageDescription(question.getString("imageDescription"));
                    if (question.has("duration")) question1.setDuration(question.optLong("duration"));
                    if (question.has("answerId")) question1.setAnswerId(question.getString("answerId"));
                    List<String> bucketIdsList = new ArrayList<>();
                    List<QuestionOption> questionOptionsList = new ArrayList<>();
                    if (question.get("options").toString().equals("")) {
                        question1.setOptions(new ArrayList<>());
                    } else {
                        JSONArray optionsArray = question.getJSONArray("options");
                        for (Object option : optionsArray) {
                            if (!option.toString().equals("null")) {
                                JSONObject option1 = new JSONObject(option.toString());
                                QuestionOption questionOption = new QuestionOption(option1.getString("optionId"),
                                        option1.getBoolean("isOptionAnImage"));
                                if (option1.has("optionText")) questionOption.setOptionText(
                                        option1.getString("optionText"));
                                if (option1.has("pathToOptionImage")) {
                                    questionOption.setPathToOptionImage(option1.getString("pathToOptionImage"));
                                }
                                questionOptionsList.add(questionOption);
                            }
                        }
                        question1.setOptions(questionOptionsList);
                    }
                    if (question.get("bucketIds").toString().equals("")) {
                        question1.setBucketIds(new ArrayList<>());
                    } else {
                        JSONArray bucketIdsArray = question.getJSONArray("bucketIds");
                        for (Object bucketId : bucketIdsArray) {
                            bucketIdsList.add(bucketId.toString());
                            MongoDBController.fetchFileData(new ObjectId(bucketId.toString()));
                        }
                        question1.setBucketIds(bucketIdsList);
                    }
                    questionArrayList.add(question1);
                }
                task1.setQuestions(questionArrayList);
            }
            if (currTask.get("files").toString().equals("")) task1.setFiles(new ArrayList<>());
            else {
                JSONArray fileArray = currTask.getJSONArray("files");
                for (Object file : fileArray) {
                    JSONObject files = new JSONObject(file.toString());
                    Files file1 = new Files(files.getString("fileName"), files.getString("filePath"));
                    fileList.add(file1);
                }
                task1.setFiles(fileList);
            }
            allTasks.add(task1);
        }
        return allTasks;
    }

    public static ArrayList<Payment> getPaymentsArray(List<Document> payments) {
        ArrayList<Payment> allPayments = new ArrayList<>();
        for (Document payment : payments) {
            JSONObject currPayment = new JSONObject(payment.toJson());
            Payment payment1 = new Payment(currPayment.getString("_id"), currPayment.getString("taskid"),
                    currPayment.getString("harvester"), PaymentMethod.valueOf(currPayment.getString("harvestermethod")),
                    currPayment.getString("reviewer"), PaymentMethod.valueOf(currPayment.getString("reviewermethod")),
                    currPayment.getDouble("amount"), Currency.getInstance(currPayment.getString("currency")),
                    PaymentStatus.valueOf(currPayment.getString("status")));
            allPayments.add(payment1);
        }
        return allPayments;
    }

    public static void initializePayment(Payment payment, User user) {
        Transfers transfers = new Transfers();
        JSONObject transferRecipient = transfers.createTransferRecipient((user.getFirstName() + " " + user.getLastName()),
                user.getAccountNumber(), user.getBankCode());
        System.out.println(transferRecipient.toString());
        JSONObject transfer = transfers.initializeTransfer(String.valueOf(payment.getAmount() * 100),
                transferRecipient.getString("recipient_code"), payment.getTaskId());
        System.out.println(transfer.toString());
        JSONObject transferFinalize = transfers.finalizeTransfer(transfer.getString("transfer_code"), generateOtp());
        System.out.println(transferFinalize.toString());
        JSONObject transferVerify = transfers.verifyTransfer(transferFinalize.getString("reference"));
        System.out.println(transferVerify);
    }

    private static String generateOtp() {
        String numbers = "0123456789";

        Random rand = new Random();

        StringBuilder otp = new StringBuilder();

        for (int i = 0; i < 6; i++) {
            otp.append(rand.nextInt(numbers.length()));
        }
        return otp.toString();
    }
}
