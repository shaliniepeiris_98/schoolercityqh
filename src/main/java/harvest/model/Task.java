package harvest.model;

import org.bson.types.ObjectId;

import java.util.*;

public class Task {

    private String taskId;
    private String title;
    private String subTitle;
    private int numOfQuestions;
    private Double pricePerQuestion;
    private String details;
    private Currency currency;
    private Map<String, List<String>> QuestionIDs = new HashMap<>();
    private List<Question> Questions = new ArrayList<>();
    private String harvester;
    private String reviewer;
    private TaskStatus status;
    private List<Files> files;

    public Task(String title, String subTitle, int numOfQuestions, Double pricePerQuestion, Currency currency, TaskStatus status) {
        this.taskId = String.valueOf(new ObjectId());
        this.title = title;
        this.subTitle = subTitle;
        this.numOfQuestions = numOfQuestions;
        this.pricePerQuestion = pricePerQuestion;
        this.currency = currency;
        this.status = status;
    }

    public Task(String id, String title, String subtitle, int questionno, double costperquestion, Currency currency, TaskStatus status) {
        this.taskId = id;
        this.title = title;
        this.subTitle = subtitle;
        this.numOfQuestions = questionno;
        this.pricePerQuestion = costperquestion;
        this.currency = currency;
        this.status = status;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public int getNumOfQuestions() {
        return numOfQuestions;
    }

    public void setNumOfQuestions(int numOfQuestions) {
        this.numOfQuestions = numOfQuestions;
    }

    public Double getPricePerQuestion() {
        return pricePerQuestion;
    }

    public void setPricePerQuestion(Double pricePerQuestion) {
        this.pricePerQuestion = pricePerQuestion;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Map<String, List<String>> getQuestionIDs() { return QuestionIDs; }

    public void setQuestionId(String questionId, List<String> options){
        this.QuestionIDs.put(questionId, options);
    }

    public void setQuestionIDs(Map<String, List<String>> questionIDs) { QuestionIDs = questionIDs; }

    public List<Question> getQuestions() {
        return Questions;
    }

    public void setQuestions(List<Question> questions) {
        Questions = questions;
    }

    public String getHarvester() {
        return harvester;
    }

    public void setHarvester(String harvester) {
        this.harvester = harvester;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public List<Files> getFiles() {
        return files;
    }

    public void setFiles(List<Files> files) {
        this.files = files;
    }
}