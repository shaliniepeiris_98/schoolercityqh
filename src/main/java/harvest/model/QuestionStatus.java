package harvest.model;

public enum QuestionStatus {
    Approved,
    Rejected,
    RequestChange,
}
