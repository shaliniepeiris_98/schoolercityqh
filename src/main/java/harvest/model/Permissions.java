package harvest.model;

public class Permissions {
    private boolean CreateTask;
    private boolean UserOngoingTasks;
    private boolean Requests;
    private boolean CompleteTasks;
    private boolean CompletedTasks;
    private boolean UserPayments;
    private boolean PendingPayments;
    private boolean PromoteUsers;
    private boolean AllTasks;
    private boolean AvailableTasks;

    public void setAdminPermissions() {
        setAvailableTasks(true);
        setCreateTask(true);
        setRequests(true);
        setUserOngoingTasks(true);
        setCompleteTasks(true);
        setCompletedTasks(true);
        setUserPayments(true);
        setPendingPayments(true);
        setPromoteUsers(true);
        setAllTasks(true);
    }

    public void setReviewerPermissions() {
        setAvailableTasks(true);
        setCreateTask(false);
        setUserOngoingTasks(true);
        setRequests(false);
        setCompleteTasks(false);
        setCompletedTasks(true);
        setUserPayments(true);
        setPendingPayments(false);
        setPromoteUsers(false);
        setAllTasks(false);
    }

    public void setHarvesterPermissions() {
        setAvailableTasks(true);
        setCreateTask(false);
        setUserOngoingTasks(true);
        setRequests(false);
        setCompleteTasks(false);
        setCompletedTasks(true);
        setUserPayments(true);
        setPendingPayments(false);
        setPromoteUsers(false);
        setAllTasks(false);
    }

    public boolean isCreateTask() {
        return CreateTask;
    }

    private void setCreateTask(boolean createTask) {
        CreateTask = createTask;
    }

    public boolean isUserOngoingTasks() {
        return UserOngoingTasks;
    }

    private void setUserOngoingTasks(boolean userOngoingTasks) {
        UserOngoingTasks = userOngoingTasks;
    }

    public boolean isRequests() {
        return Requests;
    }

    public void setRequests(boolean requests) {
        Requests = requests;
    }

    public boolean isCompleteTasks() {
        return CompleteTasks;
    }

    private void setCompleteTasks(boolean completeTasks) {
        CompleteTasks = completeTasks;
    }

    public boolean isUserPayments() {
        return UserPayments;
    }

    public void setUserPayments(boolean payments) {
        UserPayments = payments;
    }

    public boolean isPendingPayments() {
        return PendingPayments;
    }

    public void setPendingPayments(boolean pendingPayments) {
        PendingPayments = pendingPayments;
    }

    public boolean isPromoteUsers() {
        return PromoteUsers;
    }

    public void setPromoteUsers(boolean promoteUsers) {
        PromoteUsers = promoteUsers;
    }

    public boolean isAllTasks() {
        return AllTasks;
    }

    public void setAllTasks(boolean allTasks) {
        AllTasks = allTasks;
    }

    public boolean isAvailableTasks() {
        return AvailableTasks;
    }

    private void setAvailableTasks(boolean availableTasks) {
        AvailableTasks = availableTasks;
    }

    public boolean isCompletedTasks() {
        return CompletedTasks;
    }

    public void setCompletedTasks(boolean completedTasks) {
        CompletedTasks = completedTasks;
    }
}
