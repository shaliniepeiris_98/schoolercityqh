package harvest.model;

public enum PaymentMethod {
    Bank,
    Direct,
    Paystack
}
