package harvest.model;

public enum Role {
    Admin,
    Reviewer,
    Harvester
}
