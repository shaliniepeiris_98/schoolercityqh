package harvest.model;

public enum TaskStatus {
    Created,
    HarvestRequested,
    Harvesting,
    Harvested,
    ReviewRequested,
    Reviewing,
    Reviewed,
    Completed,
    Closed

}
