package harvest.model;

import org.bson.types.ObjectId;

public class Files {
    private ObjectId fileId;
    private String fileName;
    private String filePath;
    private ObjectId bucketId;

    public Files(String fileName, String filePath) {
        this.fileId = new ObjectId();
        this.fileName = fileName;
        this.filePath = filePath;
    }

    public ObjectId getFileId() {
        return fileId;
    }

    public void setFileId(ObjectId fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public ObjectId getBucketId() {
        return bucketId;
    }

    public void setBucketId(ObjectId bucketId) {
        this.bucketId = bucketId;
    }
}
