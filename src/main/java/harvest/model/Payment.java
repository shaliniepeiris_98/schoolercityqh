package harvest.model;

import java.util.Currency;

public class Payment {
    private String paymentId;
    private String taskId;
    private String harvesterId;
    private PaymentMethod harvesterMethod;
    private String reviewerId;
    private PaymentMethod reviewerMethod;
    private Double amount;
    private Currency currency;
    private PaymentStatus paymentState;

    public Payment(String paymentId, String taskId, String harvesterId, PaymentMethod harvesterMethod, String reviewerId,
                   PaymentMethod reviewerMethod, Double amount, Currency currency, PaymentStatus paymentState) {
        this.paymentId = paymentId;
        this.taskId = taskId;
        this.harvesterId = harvesterId;
        this.harvesterMethod = harvesterMethod;
        this.reviewerId = reviewerId;
        this.reviewerMethod = reviewerMethod;
        this.amount = amount;
        this.currency = currency;
        this.paymentState = paymentState;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getHarvesterId() {
        return harvesterId;
    }

    public void setHarvesterId(String harvesterId) {
        this.harvesterId = harvesterId;
    }

    public PaymentMethod getHarvesterMethod() {
        return harvesterMethod;
    }

    public void setHarvesterMethod(PaymentMethod harvesterMethod) {
        this.harvesterMethod = harvesterMethod;
    }

    public String getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(String reviewerId) {
        this.reviewerId = reviewerId;
    }

    public PaymentMethod getReviewerMethod() {
        return reviewerMethod;
    }

    public void setReviewerMethod(PaymentMethod reviewerMethod) {
        this.reviewerMethod = reviewerMethod;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public PaymentStatus getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(PaymentStatus paymentState) {
        this.paymentState = paymentState;
    }
}
