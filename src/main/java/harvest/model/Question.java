package harvest.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Question {

    private String questionId;
    private String question;
    private String instructions;
    private String registeredBy;
    private long createdOn;
    private List<QuestionOption> options;
    private String pathToImage;
    private String imageDescription;
    private long duration; // Milliseconds
    private String answerId;
    private List<String> bucketIds;
    private QuestionStatus status;
    private String changeRequest;

    public Question() {
        this.questionId = UUID.randomUUID().toString();
        this.createdOn = Instant.now().toEpochMilli();
        this.bucketIds = new ArrayList<>();
        this.options = new ArrayList<>();
    }

    public Question(String questionId) {
        this.questionId = questionId;
        this.createdOn = Instant.now().toEpochMilli();
        this.bucketIds = new ArrayList<>();
        this.options = new ArrayList<>();
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public List<QuestionOption> getOptions() {
        return options;
    }

    public void setOptions(List<QuestionOption> options) {
        this.options = options;
    }

    public String getPathToImage() {
        return pathToImage;
    }

    public void setPathToImage(String pathToImage) {
        this.pathToImage = pathToImage;
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getRegisteredBy() {
        return registeredBy;
    }

    public void setRegisteredBy(String registeredBy) {
        this.registeredBy = registeredBy;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    public List<String> getBucketIds() {
        return bucketIds;
    }

    public void setBucketIds(List<String> bucketIds) {
        this.bucketIds = bucketIds;
    }

    public void addBucketId(String id) {
        if (id == null || id.isEmpty()) {
            return;
        }

        if (this.bucketIds == null) {
            this.bucketIds = new ArrayList<>();
        }

        this.bucketIds.add(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Question)) return false;
        Question question1 = (Question) o;
        return getCreatedOn() == question1.getCreatedOn()
                && getQuestionId().equals(question1.getQuestionId())
                && Objects.equals(getQuestion(), question1.getQuestion())
                && Objects.equals(getInstructions(), question1.getInstructions())
                && Objects.equals(getRegisteredBy(), question1.getRegisteredBy())
                && Objects.equals(getOptions(), question1.getOptions())
                && Objects.equals(getPathToImage(), question1.getPathToImage())
                && Objects.equals(getImageDescription(), question1.getImageDescription())
                && Objects.equals(getDuration(), question1.getDuration())
                && Objects.equals(getAnswerId(), question1.getAnswerId())
                && Objects.equals(getBucketIds(), question1.getBucketIds());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuestionId(), getQuestion(),
                getInstructions(), getRegisteredBy(), getCreatedOn(),
                getOptions(), getPathToImage(), getImageDescription(),
                getDuration(), getAnswerId(), getBucketIds());
    }

    @Override
    public String toString() {
        return "Question{" +
                "questionId='" + questionId + '\'' +
                ", question='" + question + '\'' +
                ", instructions='" + instructions + '\'' +
                ", registeredBy='" + registeredBy + '\'' +
                ", createdOn=" + createdOn +
                ", options=" + options +
                ", pathToImage='" + pathToImage + '\'' +
                ", imageDescription='" + imageDescription + '\'' +
                ", duration=" + duration +
                ", answerId='" + answerId + '\'' +
                ", bucketIds=" + bucketIds +
                '}';
    }

    public QuestionStatus getStatus() {
        return status;
    }

    public void setStatus(QuestionStatus status) {
        this.status = status;
    }

    public String getChangeRequest() {
        return changeRequest;
    }

    public void setChangeRequest(String changeRequest) {
        this.changeRequest = changeRequest;
    }
}

