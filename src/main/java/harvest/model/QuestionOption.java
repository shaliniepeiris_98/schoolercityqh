package harvest.model;

import java.util.Objects;
import java.util.UUID;

public class QuestionOption {

    private String optionId;
    private String optionText;
    private String pathToOptionImage;
    private boolean isOptionAnImage;

    public QuestionOption() {
        optionId = UUID.randomUUID().toString();
        isOptionAnImage = false;
    }

    public QuestionOption(String option, Boolean isImage) {
        optionId = option;
        isOptionAnImage = isImage;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getPathToOptionImage() {
        return pathToOptionImage;
    }

    public void setPathToOptionImage(String pathToOptionImage) {
        this.pathToOptionImage = pathToOptionImage;
    }

    public boolean getIsOptionAnImage() {
        return isOptionAnImage;
    }

    public void setIsOptionAnImage(boolean isOptionAnImage) {
        this.isOptionAnImage = isOptionAnImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QuestionOption)) return false;
        QuestionOption that = (QuestionOption) o;
        return isOptionAnImage == that.isOptionAnImage
                && getOptionId().equals(that.getOptionId())
                && Objects.equals(getOptionText(), that.getOptionText())
                && Objects.equals(getPathToOptionImage(), that.getPathToOptionImage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOptionId(), getOptionText(), getPathToOptionImage(), isOptionAnImage);
    }

    @Override
    public String toString() {
        return "QuestionOption{" +
                "optionId='" + optionId + '\'' +
                ", optionText='" + optionText + '\'' +
                ", pathToOptionImage='" + pathToOptionImage + '\'' +
                ", isOptionAnImage=" + isOptionAnImage +
                '}';
    }
}
