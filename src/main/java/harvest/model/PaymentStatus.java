package harvest.model;

public enum PaymentStatus {
    Pending,
    PartiallyCompleted,
    Completed
}
