package harvest.paymentAPI;

import org.json.JSONObject;

public class Transfers {

    private ApiConnection apiConnection;

    /**
     * Used to create a transfer recipient
     *
     * @param name User Name
     * @param accountNumber User Account Number
     * @param bankCode User Bank Code
     * @return Transfer Recipient Object
     */
    public JSONObject createTransferRecipient(String name, String accountNumber, String bankCode) {
        this.apiConnection = new ApiConnection(Definitions.PAYSTACK_TRANSFER_CREATE_TRANSFER_RECIPIENT);
        ApiQuery apiQuery = new ApiQuery();
        apiQuery.putParams("type", "nuban");
        apiQuery.putParams("name", name);
        apiQuery.putParams("bank_account", accountNumber);
        apiQuery.putParams("bank_code", bankCode);
        return this.apiConnection.connectAndQuery(apiQuery);
    }

    /**
     * Used to initialize a transfer
     *
     * @param amount Transfer Amount
     * @param recipient Recipient of Amount
     * @param reason Purpose of Transfer
     * @return Transfer Object
     */
    public JSONObject initializeTransfer(String amount, String recipient, String reason) {
        this.apiConnection = new ApiConnection(Definitions.PAYSTACK_TRANSFER_INITIATE_TRANSFER);
        ApiQuery apiQuery = new ApiQuery();
        apiQuery.putParams("source", "balance");
        apiQuery.putParams("amount", amount);
        apiQuery.putParams("email", recipient);
        apiQuery.putParams("currency", reason);
        return this.apiConnection.connectAndQuery(apiQuery);
    }

    /**
     * Used to finalize a transfer
     *
     * @param transferCode Code of Transfer
     * @param otp OTP code to business phone
     * @return Transfer object an status
     */
    public JSONObject finalizeTransfer(String transferCode, String otp) {
        this.apiConnection = new ApiConnection(Definitions.PAYSTACK_TRANSFER_FINALIZE_TRANSFER);
        ApiQuery apiQuery = new ApiQuery();
        apiQuery.putParams("transfer_code", transferCode);
        apiQuery.putParams("otp", otp);
        return this.apiConnection.connectAndQuery(apiQuery);
    }

    /**
     * Used to verify a transfer
     *
     * @param transferReference Transfer Reference
     * @return Transfer Object Verification
     */
    public JSONObject verifyTransfer(String transferReference) {
        this.apiConnection = new ApiConnection(Definitions.PAYSTACK_TRANSFER_VERIFY_TRANSFER);
        ApiQuery apiQuery = new ApiQuery();
        apiQuery.putParams("transfer_code", transferReference);
        return this.apiConnection.connectAndQuery(apiQuery);
    }


}
