package harvest.paymentAPI;

import java.util.HashMap;

public class ApiQuery {

    private HashMap<String, Object> queryMap;

    /**
     * Initializes a new query map
     */
    ApiQuery() {
        this.queryMap = new HashMap<>();
    }

    /**
     * Used to add a parameter to the query map
     *
     * @param key Query key
     * @param value Query value
     */
    void putParams(String key, Object value) {
        this.queryMap.put(key, value);
    }

    /**
     * Used to get all parameters within the query map
     *
     * @return - HashMap containing query parameters
     */
    public HashMap<String, Object> getParams() {
        return this.queryMap;
    }

}
