package harvest.paymentAPI;

class Definitions {

    private final static String BASE_API_ENDPOINT = "https://api.paystack.co";

    //URL definitions for transfer endpoint
    static final String PAYSTACK_TRANSFER_CREATE_TRANSFER_RECIPIENT = BASE_API_ENDPOINT + "/transferrecipient";
    static final String PAYSTACK_TRANSFER_INITIATE_TRANSFER = BASE_API_ENDPOINT + "/transfer";
    static final String PAYSTACK_TRANSFER_FINALIZE_TRANSFER = BASE_API_ENDPOINT + "/transfer/finalize_transfer";
    static final String PAYSTACK_TRANSFER_VERIFY_TRANSFER = BASE_API_ENDPOINT + "/transfer/verify/";

}
