package harvest.controller;

import harvest.Utility;
import harvest.model.PaymentMethod;
import harvest.model.User;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

public class ProfileController implements Initializable {
    @FXML
    PasswordField passwordField, confirmPasswordField;

    @FXML
    Button passwordButton, confirmPasswordButton, saveProfileButton;

    @FXML
    Label roleLabel, nameLabel, usernameLabel;

    @FXML
    TextField emailTextField, phoneNoTextField, passwordTextField, confirmPasswordTextField;

    @FXML
    ChoiceBox<String> countryChoiceBox;

    @FXML
    VBox paymentMethodDetailsPane;

    @FXML
    FlowPane paymentMethodOptionPane;

    static User user1;
    private User user;
    private PaymentMethod[] paymentMethods = {PaymentMethod.Direct, PaymentMethod.Bank, PaymentMethod.Paystack};
    private Map<String, List<String>> banks = new HashMap<>();
    private List<String> bankcountries = new ArrayList<>();
    private Map<String, String> bankCodes = new HashMap<>();
    private List<String> countries = new ArrayList<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            getBanksJson();
            loadCountries();
        } catch (IOException e) {
            e.printStackTrace();
        }
        user = MongoDBController.loadUserDetails(user1);
        roleLabel.setText(String.valueOf(user.getRole()));
        nameLabel.setText(user.getFirstName() + " " + user.getLastName());
        usernameLabel.setText(user.getUserName());
        emailTextField.setText(user.getEmail());
        phoneNoTextField.setText("+" + user.getPhoneNumber());
        passwordField.setText(user.getPassword());
        confirmPasswordField.setText(user.getPassword());
        passwordTextField.setText(user.getPassword());
        passwordField.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextField.setText(passwordField.getText());
        });
        confirmPasswordField.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmPasswordTextField.setText(confirmPasswordField.getText());
        });
        passwordTextField.visibleProperty().bind(passwordButton.armedProperty());
        confirmPasswordTextField.visibleProperty().bind(confirmPasswordButton.armedProperty());
        phoneNoTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\+\\d*")) {
                phoneNoTextField.setText(newValue.replaceAll("[^+\\d]", ""));
            }
        });
        confirmPasswordTextField.setText(user.getPassword());
        countryChoiceBox.setValue(user.getCountry());
        countryChoiceBox.setItems(FXCollections.observableList(countries));
        countryChoiceBox.setOnAction((event) -> {
            user.setCountry(countryChoiceBox.getValue());
        });
        setPaymentOptions();
    }

    private void setPaymentOptions() {
        ToggleGroup tg = new ToggleGroup();
        for (int i = 0; i < 3; i++) {
            ToggleButton tb = new ToggleButton(String.valueOf(paymentMethods[i]));
            int finalI = i;
            if (user.getPaymentMethod().equals(paymentMethods[i])) {
                tb.setSelected(true);
                setDetailsSection(paymentMethods[i]);
            }
            tb.setOnAction(e -> {
                user.setPaymentMethod(paymentMethods[finalI]);
                setDetailsSection(paymentMethods[finalI]);
                changePaymentMethod(paymentMethods[finalI]);
            });
            tb.setToggleGroup(tg);
            paymentMethodOptionPane.getChildren().add(tb);
        }
    }

    private void setDetailsSection(PaymentMethod paymentMethod) {
        if (paymentMethod.equals(PaymentMethod.Bank)) {
            setBankInput();
        } else if (paymentMethod.equals(PaymentMethod.Paystack)) {
            setPaystackInput();
        } else {
            paymentMethodDetailsPane.getChildren().clear();
            Label label = new Label("Admin will use phone number for this option");
            user.setBankName(null);
            user.setBankCountry(null);
            user.setBankCode(null);
            user.setAccountNumber(null);
            paymentMethodDetailsPane.getChildren().add(label);
        }
    }

    private void setPaystackInput() {
        paymentMethodDetailsPane.getChildren().clear();
        ChoiceBox<String> bankCountryChoiceBox = new ChoiceBox<>();
        bankCountryChoiceBox.setPrefWidth(300.0);
        bankCountryChoiceBox.setItems(FXCollections.observableArrayList(bankcountries));
        ChoiceBox<String> bankSelection = new ChoiceBox<>();
        bankSelection.setPrefWidth(300.0);
        TextField accountTextFeild = new TextField();
        accountTextFeild.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                accountTextFeild.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        accountTextFeild.setPrefWidth(300.0);
        if (user.getPaymentMethod().equals(PaymentMethod.Paystack)) {
            if (user.getBankCountry() != null) {
                bankCountryChoiceBox.setValue(user.getBankCountry());
                bankSelection.setItems(FXCollections.observableList(banks.get(user.getBankCountry())));
            }
            if (user.getBankName() != null)
                bankSelection.setValue(user.getBankName());
            if (user.getAccountNumber() != null)
                accountTextFeild.setText(user.getAccountNumber());
        }
        bankCountryChoiceBox.setOnAction(e -> {
            user.setBankCountry(bankCountryChoiceBox.getValue());
            bankSelection.setItems(FXCollections.observableList(banks.get(bankCountryChoiceBox.getValue())));
        });
        bankSelection.setOnAction(e -> {
            if (bankCountryChoiceBox.getValue() != null) {
                user.setBankName(bankSelection.getValue());
            } else {
                Utility.showAlert(Alert.AlertType.WARNING, "Please set a bank country to select a bank.");
            }
        });
        Label countryLabel = new Label("Select your paystack bank country from the choice box below.");
        Label bankLabel = new Label("Select your paystack bank from the choice box below.");
        Label accountLabel = new Label("Paystack Account Number: ");
        accountTextFeild.setPromptText("Enter paystack account number here.");
        HBox bankHbox = new HBox();
        bankHbox.getChildren().addAll(accountLabel, accountTextFeild);
        paymentMethodDetailsPane.getChildren().addAll(countryLabel, bankCountryChoiceBox, bankLabel, bankSelection, bankHbox);
    }

    private void setBankInput() {
        paymentMethodDetailsPane.getChildren().clear();
        ChoiceBox<String> bankCountryChoiceBox = new ChoiceBox<>();
        bankCountryChoiceBox.setPrefWidth(300.0);
        bankCountryChoiceBox.setItems(FXCollections.observableArrayList(bankcountries));
        ChoiceBox<String> bankSelection = new ChoiceBox<>();
        bankSelection.setPrefWidth(300.0);
        TextField accountTextFeild = new TextField();
        accountTextFeild.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                accountTextFeild.setText(newValue.replaceAll("[^\\d]", ""));
            }
            user.setAccountNumber(accountTextFeild.getText());
        });
        accountTextFeild.setPrefWidth(250);
        if (user.getPaymentMethod().equals(PaymentMethod.Bank)) {
            if (user.getBankCountry() != null) {
                bankCountryChoiceBox.setValue(user.getBankCountry());
                bankSelection.setItems(FXCollections.observableList(banks.get(user.getBankCountry())));
            }
            if (user.getBankName() != null)
                bankSelection.setValue(user.getBankName());
            if (user.getAccountNumber() != null)
                accountTextFeild.setText(user.getAccountNumber());
        }
        bankCountryChoiceBox.setOnAction(e -> {
            user.setBankCountry(bankCountryChoiceBox.getValue());
            bankSelection.setItems(FXCollections.observableList(banks.get(bankCountryChoiceBox.getValue())));
        });
        bankSelection.setOnAction(e -> {
            if (bankCountryChoiceBox.getValue() != null) {
                user.setBankName(bankSelection.getValue());
            } else {
                Utility.showAlert(Alert.AlertType.WARNING, "Please set a bank country to select a bank.");
            }
        });
        Label countryLabel = new Label("Select your bank country from the choice box below.");
        Label bankLabel = new Label("Select your bank from the choice box below.");
        Label accountLabel = new Label("Account Number: ");
        accountTextFeild.setPromptText("Enter account number here.");
        HBox bankHbox = new HBox();
        bankHbox.getChildren().addAll(accountLabel, accountTextFeild);
        paymentMethodDetailsPane.getChildren().addAll(countryLabel, bankCountryChoiceBox, bankLabel, bankSelection, bankHbox);
    }

    private void getBanksJson() throws IOException {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("BankCodes.json")) {
            Object obj = jsonParser.parse(reader);
            JSONArray bankList = (JSONArray) obj;
            for (Object bank : bankList) {
                JSONObject newBank = new JSONObject(bank.toString());
                if (!bankcountries.contains(newBank.getString("country"))) {
                    bankcountries.add(newBank.getString("country"));
                }
                if (!banks.containsKey(newBank.getString("country"))) {
                    List<String> countryBanks = new ArrayList<>();
                    countryBanks.add(newBank.getString("name"));
                    banks.put(newBank.getString("country"), countryBanks);
                } else {
                    banks.get(newBank.getString("country")).add(newBank.getString("name"));
                }
                bankCodes.put(newBank.getString("code"), newBank.getString("name"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void loadCountries() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("countries.json")) {
            Object obj = jsonParser.parse(reader);

            JSONArray bankList = (JSONArray) obj;
            for (Object bank : bankList) {
                JSONObject newBank = new JSONObject(bank.toString());
                countries.add(newBank.getString("name"));
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    private void changePaymentMethod(PaymentMethod paymentMethod) {
        user.setPaymentMethod(paymentMethod);
    }

    public void saveProfile() {
        if (changesValid()) {
            getBankCode();
            MongoDBController.changeUser(user);
            Utility.showAlert(Alert.AlertType.INFORMATION, "User has been updated.");
        }
    }

    private void getBankCode() {
        for (String bankCode : bankCodes.keySet()) {
            if (bankCodes.get(bankCode).equals(user.getBankName())) {
                user.setBankCode(bankCode);
            }
        }
    }

    private boolean changesValid() {
        if (!user.getPaymentMethod().equals(PaymentMethod.Direct)) {
            if (user.getBankName() == null || user.getBankName().equals("")) {
                Utility.showAlert(Alert.AlertType.WARNING, "Bank name is invalid. Please enter bank name");
                return false;
            } else if (user.getBankCountry() == null || user.getBankCountry().equals("")) {
                Utility.showAlert(Alert.AlertType.WARNING, "Bank country is invalid. Please enter bank country");
                return false;
            } else if (user.getAccountNumber() == null || user.getAccountNumber().equals("")) {
                Utility.showAlert(Alert.AlertType.WARNING, "Bank account number is invalid. Please enter bank account number");
                return false;
            }
        }
        if (!emailIsValid(emailTextField.getText()) || emailTextField.getText().isEmpty() || emailTextField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Email is invalid. Please enter a correct email.");
            return false;
        } else if (phoneNoTextField.getText().isEmpty() || phoneNoTextField.getText().equals(" ") || !numberIsValid(phoneNoTextField.getText())) {
            Utility.showAlert(Alert.AlertType.WARNING, "Phone number is invalid. Please enter a correct number starting with \"+[country code]\".");
            return false;
        } else if (passwordField.getText().isEmpty() || passwordField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Password is invalid. Please enter a correct password.");
            return false;
        } else if (confirmPasswordField.getText().isEmpty() || confirmPasswordField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Password is invalid. Please enter a correct password.");
            return false;
        } else if (!confirmPasswordField.getText().equals(passwordField.getText())) {
            Utility.showAlert(Alert.AlertType.WARNING, "Passwords do not match. Please enter matching passwords to confirm");
            return false;
        }
        return true;
    }

    private boolean numberIsValid(String text) {
        String phoneRegex = "\\+(9[9876543210]\\d|8[987653210]\\d|7[9876543210]\\d|6[9876543210]\\d|5[9876543210]\\d|" +
                "4[9876543210]\\d|3[9876543210]\\d|2[9876543210]\\d|1[9876432]\\d)\\d{1,14}$";
        Pattern pat = Pattern.compile(phoneRegex);
        if (text == null) return false;
        return pat.matcher(text).matches();
    }

    private boolean emailIsValid(String text) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (text == null) return false;
        return pat.matcher(text).matches();
    }
}
