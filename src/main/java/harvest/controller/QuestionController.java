package harvest.controller;

import harvest.Utility;
import harvest.model.Question;
import harvest.model.QuestionOption;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class QuestionController implements Initializable {
    private ObservableList<Integer> answerOptions = FXCollections.observableArrayList(1, 2, 3, 4, 5, 6, 7);
    private FileChooser fileChooser = new FileChooser();
    static int questionNo;
    private String folderPath = System.getProperty("user.dir") + "src\\main\\resources\\questionImages\\";

    @FXML
    Label questionLabel, statusLabel;

    @FXML
    TextField questionTextField;

    @FXML
    TextArea instructionTextArea;

    @FXML
    VBox questionAnswers, questionVBox;

    @FXML
    AnchorPane questionAnchorPane;

    @FXML
    FlowPane filePane;

    @FXML
    ChoiceBox<Integer> questionAnswerNumber;

    @FXML
    Button addFilesButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        questionLabel.setText("Question No. " + (questionNo + 1));
        setQuestionCheck();
        questionAnswerNumber.setItems(answerOptions);
        questionAnswerNumber.setOnAction((event) -> {
            if (HarvestQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo).isEmpty()) {
                Utility.showAlert(Alert.AlertType.WARNING, "Please enter a Question to set number of answers");
                questionAnswerNumber.setValue(null);
            } else {
                setAnswerSection(questionAnswerNumber.getValue());
            }
        });
    }

    private void setQuestionCheck() {
        Question question = new Question();
        String questionID;
        questionID = HarvestQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : HarvestQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        questionTextField.setText(question.getQuestion());
        instructionTextArea.setText(question.getInstructions());
        if (question.getStatus() != null && question.getChangeRequest() != null)
            statusLabel.setText(question.getStatus().toString() + " - " + question.getChangeRequest());
        else if (question.getStatus() != null) statusLabel.setText(question.getStatus().toString());
        if (question.getPathToImage() != null) {
            fileAddon(new File(question.getPathToImage()), question);
        }
        if (!question.getOptions().isEmpty()) {
            questionAnswerNumber.setValue(question.getOptions().size());
            setAnswerSection(question.getOptions().size());
        }
        Question finalQuestion = question;
        questionTextField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                saveQuestion(finalQuestion);
            }
        });
        instructionTextArea.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                saveQuestion(finalQuestion);
            }
        });

    }

    private void saveQuestion(Question question) {
        if ((questionTextField.getText() != null && !questionTextField.getText().equals("")) ||
                (instructionTextArea.getText() != null && !instructionTextArea.getText().equals("")) ||
                !filePane.getChildren().isEmpty()) {
            question.setQuestion(questionTextField.getText());
            question.setInstructions(instructionTextArea.getText());
            HarvestQuestionsController.task.getQuestionIDs().get("Questions").set(questionNo, question.getQuestionId());
            List<Question> list;
            if (HarvestQuestionsController.task.getQuestions().isEmpty()) {
                list = new ArrayList<>();
                list.add(question);
            } else {
                list = HarvestQuestionsController.task.getQuestions();
                if (!list.contains(question)) {
                    list.add(question);
                }
            }
            HarvestQuestionsController.task.setQuestions(list);
        }
    }

    private void setAnswerSection(int value) {
        questionAnswers.getChildren().clear();
        Question question = new Question();
        String questionID;
        questionID = HarvestQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : HarvestQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        List<QuestionOption> list = question.getOptions();
        if (list.isEmpty()) {
            for (int i = 0; i < value; i++) {
                list.add(null);
            }
        } else {
            if (list.size() <= value) {
                for (int i = list.size(); i < value; i++) {
                    list.add(null);
                }
            } else {
                list = list.subList(0, value);
                question.setOptions(list);
                question.setAnswerId(null);
                if (!HarvestQuestionsController.task.getQuestions().get(questionNo).getQuestionId().equals(question.getQuestionId())) {
                    HarvestQuestionsController.task.getQuestions().add(questionNo, question);
                } else {
                    HarvestQuestionsController.task.getQuestions().set(questionNo, question);
                }
            }
        }
        for (int i = 0; i < value; i++) {
            TextField answer = new TextField();
            answer.setPrefWidth(800);
            Button image = new Button("Add image instead");
            HBox hbox = new HBox();
            hbox.getChildren().addAll(answer, image);
            if (list.get(i) != null) {
                if (list.get(i).getIsOptionAnImage()) {
                    answer.setDisable(true);
                    image.setDisable(true);
                    try {
                        hbox.getChildren().add(addImage(i, answer, image, hbox));
                    } catch (Exception ignored) {
                    }
                } else {
                    answer.setText(list.get(i).getOptionText());
                }
            }
            int finalI = i;
            Question finalQuestion = question;
            answer.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                if (!newPropertyValue) {
                    saveAnswer(answer, finalI, finalQuestion);
                }
            });
            image.setOnAction(e -> {
                answer.clear();
                answer.setDisable(true);
                image.setDisable(true);
                hbox.getChildren().add(addImage(finalI, answer, image, hbox));
            });
            hbox.setPadding(new Insets(10, 10, 10, 10));
            questionAnswers.getChildren().addAll(hbox);
        }
        selectCorrectAnswer(value, question);
    }

    private void selectCorrectAnswer(int value, Question question) {
        HBox hBox = new HBox();
        Label label = new Label("Select correct answer from the above options:  ");
        hBox.getChildren().add(label);
        ToggleGroup answers = new ToggleGroup();
        for (int i = 0; i < value; i++) {
            ToggleButton toggleButton = new ToggleButton(String.valueOf(i + 1));
            toggleButton.setToggleGroup(answers);
            if (question.getAnswerId() != null && question.getAnswerId().equals(String.valueOf(i))) {
                toggleButton.setSelected(true);
            }
            hBox.getChildren().add(toggleButton);
            int finalI = i;
            toggleButton.setOnAction(e -> {
                if (toggleButton.isSelected()) {
                    question.setAnswerId(String.valueOf(finalI));
                    if (!HarvestQuestionsController.task.getQuestions().get(questionNo).getQuestionId().equals(question.getQuestionId())) {
                        HarvestQuestionsController.task.getQuestions().add(questionNo, question);
                    } else {
                        HarvestQuestionsController.task.getQuestions().set(questionNo, question);
                    }
                }
            });
        }
        questionAnswers.getChildren().add(hBox);
    }


    private Node addImage(int index, TextField answer, Button image, HBox hbox) {
        Question question = new Question();
        String questionID;
        questionID = HarvestQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : HarvestQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        File file;
        QuestionOption a = new QuestionOption();
        if (question.getOptions().get(index) == null || !question.getOptions().get(index).getIsOptionAnImage()) {
            file = fileChooser.showOpenDialog(new Stage());
            long fileSize = file.length() / (1024);
            if (fileSize > 5) {
                Utility.showAlert(Alert.AlertType.WARNING, "File is too large for input. " +
                        "Please enter files smaller than 5KB.");
                file = null;
            }
            if (file != null) {
                try {
                    makeQuestionImage(file.getPath(), file.getName());
                    a.setIsOptionAnImage(true);
                    a.setPathToOptionImage(folderPath + file.getName());
                    a.setOptionText(file.getName());
                    question.getOptions().set(index, a);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                answer.setDisable(false);
                image.setDisable(false);
            }
        } else {
            file = new File(question.getOptions().get(index).getPathToOptionImage());
        }
        if (file != null) {
            Button button = new Button(file.getName());
            Question finalQuestion = question;
            button.setOnAction(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to remove this file?");
                alert.showAndWait().ifPresent(r -> {
                    if (r == ButtonType.OK) {
                        a.setIsOptionAnImage(false);
                        a.setPathToOptionImage(null);
                        a.setOptionText(null);
                        finalQuestion.getOptions().set(index, a);
                        answer.setDisable(false);
                        image.setDisable(false);
                        hbox.getChildren().remove(button);
                        HarvestQuestionsController.task.getQuestions().set(questionNo, finalQuestion);
                    }
                });
            });
            HarvestQuestionsController.task.getQuestions().set(questionNo, question);
            return button;
        }
        return null;
    }

    private void saveAnswer(TextField answer, int index, Question question) {
        if (answer.getText() != null && !answer.getText().equals("")) {
            QuestionOption a = new QuestionOption();
            a.setOptionText(answer.getText());
            List<QuestionOption> answers = question.getOptions();
            answers.set(index, a);
            question.setOptions(answers);
            if (!HarvestQuestionsController.task.getQuestions().get(questionNo).getQuestionId().equals(question.getQuestionId())) {
                HarvestQuestionsController.task.getQuestions().add(questionNo, question);
            } else {
                HarvestQuestionsController.task.getQuestions().set(questionNo, question);
            }
        }
    }

    public void addFiles() throws IOException {
        if (!HarvestQuestionsController.task.getQuestions().isEmpty()) {
            if (filePane.getChildren().isEmpty()) {
                Question question = new Question();
                String questionID;
                questionID = HarvestQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
                if (!questionID.equals("")) {
                    for (Question q : HarvestQuestionsController.task.getQuestions()) {
                        if (q.getQuestionId().equals(questionID)) {
                            question = q;
                        }
                    }
                }
                File file = null;
                if (question.getPathToImage() != null) {
                    file = new File(question.getPathToImage());
                } else {
                    file = fileChooser.showOpenDialog(new Stage());
                    long fileSize = file.length() / (1024);
                    if (fileSize > 5) {
                        Utility.showAlert(Alert.AlertType.WARNING, "File is too large for input. " +
                                "Please enter files smaller than 5KB.");
                        file = null;
                    }
                }
                if (file != null) {
                    makeQuestionImage(file.getPath(), file.getName());
                    question.setImageDescription(file.getName());
                    question.setPathToImage(folderPath + file.getName());
                    List<Question> list;
                    if (HarvestQuestionsController.task.getQuestions().isEmpty()) {
                        list = new ArrayList<>();
                        list.add(question);
                    } else {
                        list = HarvestQuestionsController.task.getQuestions();
                        if (!list.contains(question)) {
                            list.add(question);
                        } else list.set(questionNo, question);
                    }
                    HarvestQuestionsController.task.setQuestions(list);
                    fileAddon(file, question);
                }
            } else {
                Utility.showAlert(Alert.AlertType.WARNING, "Cannot attach another file for this question till current file is removed.");
            }
        } else
            Utility.showAlert(Alert.AlertType.ERROR, "Cannot attach a file without a question or instructions.");
    }

    private void makeQuestionImage(String path, String name) throws IOException {
        InputStream is = new FileInputStream(path);
        OutputStream os = new FileOutputStream(folderPath + name);
        byte[] buf = new byte[1024];
        int bytesRead;
        while ((bytesRead = is.read(buf)) > 0) {
            os.write(buf, 0, bytesRead);
        }
        is.close();
        os.close();
    }

    private void fileAddon(File file, Question question) {
        Button button = new Button(file.getName());
        button.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to remove this file?");
            alert.showAndWait().ifPresent(r -> {
                if (r == ButtonType.OK) {
                    question.setPathToImage(null);
                    question.setImageDescription(null);
                    filePane.getChildren().clear();
                    HarvestQuestionsController.task.getQuestions().set(questionNo, question);
                }
            });
        });
        HarvestQuestionsController.task.getQuestions().set(questionNo, question);
        filePane.getChildren().add(button);
    }
}
