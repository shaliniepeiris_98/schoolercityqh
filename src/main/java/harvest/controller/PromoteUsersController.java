package harvest.controller;

import harvest.model.Role;
import harvest.model.User;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PromoteUsersController implements Initializable {
    @FXML
    TableView<User> tableView;

    @FXML
    TableColumn<User, String> userIdColumn, usernameColumn, nameColumn, roleColumn, promoteColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<User> users;
        users = MongoDBController.getUsers();
        tableView.setItems(users);
        userIdColumn.setCellValueFactory(new PropertyValueFactory<>("userId"));
        usernameColumn.setCellValueFactory(new PropertyValueFactory<>("userName"));
        nameColumn.setCellValueFactory(cellData ->  Bindings.createStringBinding(() -> cellData.getValue().getFirstName().concat(" ").concat(cellData.getValue().getLastName())));
        roleColumn.setCellValueFactory(new PropertyValueFactory<>("role"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        Callback<TableColumn<User, String>, TableCell<User, String>> cellFactory
                = new Callback<TableColumn<User, String>, TableCell<User, String>>() {

            @Override
            public TableCell call(final TableColumn<User, String> param) {

                return new TableCell<User, String>() {

                    final Button adminButton = new Button("Admin");
                    final Button reviewerButton = new Button("Reviewer");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            User person = getTableView().getItems().get(getIndex());
                            adminButton.setOnAction(e -> {
                                try {
                                    makeAdmin(person);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            });
                            reviewerButton.setOnAction(e -> {
                                try {
                                    makeReviewer(person);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            });
                            HBox buttonsHBox = new HBox();
                            if (person.getRole() == Role.Admin) {
                                adminButton.setDisable(true);
                                reviewerButton.setDisable(true);
                            } else if (person.getRole() == Role.Reviewer) {
                                reviewerButton.setDisable(true);
                            }
                            buttonsHBox.getChildren().addAll(adminButton, reviewerButton);
                            setGraphic(buttonsHBox);
                            setText(null);
                        }
                    }
                };
            }
        };

        promoteColumn.setCellFactory(cellFactory);
    }

    private void makeReviewer(User person) throws IOException {
        MongoDBController.updateUser(person, "Reviewer");
        initialize(getClass().getResource("resources/PromoteUsers.fxml"), null);
    }

    private void makeAdmin(User person) throws IOException {
        MongoDBController.updateUser(person, "Admin");
        initialize(getClass().getResource("resources/PromoteUsers.fxml"), null);
    }
}
