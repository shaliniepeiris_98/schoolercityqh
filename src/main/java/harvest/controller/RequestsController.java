package harvest.controller;

import harvest.model.Task;
import harvest.model.TaskStatus;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class RequestsController implements Initializable {
    @FXML
    TableView<Task> tableView;

    @FXML
    TableColumn<Task, String> harvesterColumn, taskStatusColumn, taskTitleColumn,
            taskIdColumn, reviewerColumn, approveColumn, taskSubtitleColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        tasks = MongoDBController.getRequests();
        tableView.setItems(tasks);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        taskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        taskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button approveButton = new Button("Approve");
                    final Button denyButton = new Button("Deny");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            approveButton.setOnAction(e -> {
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Approve this user for this task?");
                                alert.showAndWait().ifPresent(r -> {
                                    if (r == ButtonType.OK) {
                                        if (task.getStatus().equals(TaskStatus.HarvestRequested))
                                            approveHarvester(task);
                                        else if (task.getStatus().equals(TaskStatus.ReviewRequested))
                                            approveReviewer(task);
                                        initialize(getClass().getResource("/resources/Requests.fxml"), null);
                                    }
                                });
                            });
                            denyButton.setOnAction(e -> {
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Deny this user for this task?");
                                alert.showAndWait().ifPresent(r -> {
                                    if (r == ButtonType.OK) {
                                        if (task.getStatus().equals(TaskStatus.HarvestRequested))
                                            denyHarvester(task);
                                        else if (task.getStatus().equals(TaskStatus.ReviewRequested))
                                            denyReviewer(task);
                                        initialize(getClass().getResource("/resources/Requests.fxml"), null);
                                    }
                                });
                            });
                            HBox buttonHBox = new HBox();
                            buttonHBox.getChildren().addAll(approveButton, denyButton);
                            setGraphic(buttonHBox);
                            setText(null);
                        }
                    }
                };
            }
        };
        approveColumn.setCellFactory(cellFactory);
    }

    private void denyReviewer(Task task) {
        MongoDBController.setStatus(task, TaskStatus.Harvested, task.getHarvester(), "");
    }

    private void denyHarvester(Task task) {
        MongoDBController.setStatus(task, TaskStatus.Created, "", "");
    }

    private void approveReviewer(Task task) {
        MongoDBController.setStatus(task, TaskStatus.Reviewing, task.getHarvester(), task.getReviewer());
    }

    private void approveHarvester(Task task) {
        MongoDBController.setStatus(task, TaskStatus.Harvesting, task.getHarvester(), "");
    }
}
