package harvest.controller;

import harvest.model.Payment;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class UserPaymentsController implements Initializable {
    @FXML
    TableColumn<Payment, String> paymentIdColumn, taskIdColumn, harvesterColumn, harvesterMethodColumn, reviewerColumn,
            reviewerMethodColumn, costColumn, currencyColumn, paymentStatusColumn;

    @FXML
    TableView<Payment> tableView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Payment> payments;
        payments = MongoDBController.getPayments(HomeScreenController.user);
        tableView.setItems(payments);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        paymentIdColumn.setCellValueFactory(new PropertyValueFactory<>("paymentId"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvesterId"));
        harvesterMethodColumn.setCellValueFactory(new PropertyValueFactory<>("harvesterMethod"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewerId"));
        reviewerMethodColumn.setCellValueFactory(new PropertyValueFactory<>("reviewerMethod"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        paymentStatusColumn.setCellValueFactory(new PropertyValueFactory<>("paymentState"));
    }
}
