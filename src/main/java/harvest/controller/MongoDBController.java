package harvest.controller;

import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSDownloadOptions;
import com.mongodb.client.gridfs.model.GridFSFile;
import com.mongodb.client.model.Sorts;
import harvest.App;
import harvest.Utility;
import harvest.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class MongoDBController {
    private static String connectionString = "mongodb+srv://Shalinie:PrettyPrincess_04@cluster0.rvkpv.mongodb.net/SchoolerCityQH?retryWrites=true&w=majority";
    private static ArrayList<Files> fileIds;
    private static ArrayList<Files> questionFileIds;
    private static String path = System.getProperty("user.dir") + "\\src\\main\\resources\\";
    private static MongoDatabase appDb;

    private static MongoDatabase getDatabase(){
        if(appDb == null){
            MongoClient mongoClient = MongoClients.create(connectionString);
            CodecRegistry pojoCodecRegistry = org.bson.codecs.configuration.CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), org.bson.codecs.configuration.CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            appDb = mongoClient.getDatabase("SchoolerCityQH").withCodecRegistry(pojoCodecRegistry);
        }
        return appDb;
    }

    static void loadUser(String username, String password, ActionEvent event) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
            MongoCollection<Document> userCollection = Objects.requireNonNull(getDatabase()).getCollection("Users");
            Document user1 = userCollection.find(new Document("username", username)).first();
            if (user1 == null)
                Utility.showAlert(Alert.AlertType.ERROR, "No such user exists. Please check your credentials.");
            else checkPassword(password, user1, event);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    private static void checkPassword(String password, Document user, ActionEvent event) throws IOException {
        JSONObject userOBJ = new JSONObject(user.toJson());
        if (!userOBJ.getString("password").equals(password)) {
            Utility.showAlert(Alert.AlertType.ERROR, "Password is incorrect. Please enter the correct password for this user");
        } else {
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            HomeScreenController.user = new User(userOBJ.getString("_id"), userOBJ.getString("firstname"),
                    userOBJ.getString("lastname"), userOBJ.getString("username"),
                    Role.valueOf(userOBJ.getString("role")), PaymentMethod.valueOf(userOBJ.getString("paymentmethod")));
            App.changeScene(stage, "HomeScreen", "Home Screen");
        }
    }

    static ObservableList<User> getUsers() {
        ObservableList<User> users = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> userCollection = getDatabase().getCollection("Users");
            List<Document> user1 = userCollection.find().into(new ArrayList<>());
            users = FXCollections.observableArrayList(Utility.getUserArray(user1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return users;
    }

    static void updateUser(User user, String role) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try (MongoClient mongoClient = MongoClients.create(connectionString)) {
            CodecRegistry pojoCodecRegistry = org.bson.codecs.configuration.CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), org.bson.codecs.configuration.CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            MongoDatabase appDB = mongoClient.getDatabase("SchoolerCityQH").withCodecRegistry(pojoCodecRegistry);
            MongoCollection<Document> userCollection = appDB.getCollection("Users");
            Bson filter = eq("_id", user.getUserId());
            Bson updateOperation = set("role", role);
            userCollection.updateOne(filter, updateOperation);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static void changeUser(User user) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> userCollection = getDatabase().getCollection("Users");
            if (user.getAccountNumber() == null) user.setAccountNumber("");
            if (user.getBankName() == null) user.setBankName("");
            if (user.getBankCode() == null) user.setBankCode("");
            Bson filter = eq("_id", user.getUserId());
            Bson update1 = set("password", user.getPassword());
            Bson update2 = set("paymentmethod", String.valueOf(user.getPaymentMethod()));
            Bson update3 = set("phonenumber", user.getPhoneNumber());
            Bson update4 = set("accountno", user.getAccountNumber());
            Bson update5 = set("bankname", user.getBankName());
            Bson update6 = set("country", user.getCountry());
            Bson update7 = set("email", user.getEmail());
            Bson update8 = set("bankcode", user.getBankCode());
            Bson update9 = set("bankcountry", user.getBankCountry());
            Bson updateOperation = combine(update1, update2, update3, update4, update5, update6, update7, update8, update9);
            userCollection.updateOne(filter, updateOperation);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static void saveTask(Task task) {
        ArrayList<Files> files;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try (MongoClient mongoClient = MongoClients.create(connectionString)) {
            CodecRegistry pojoCodecRegistry = org.bson.codecs.configuration.CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), org.bson.codecs.configuration.CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
            MongoDatabase appDB = mongoClient.getDatabase("SchoolerCityQH").withCodecRegistry(pojoCodecRegistry);
            GridFSBucket gridFSFilesBucket = GridFSBuckets.create(appDB, "files");
            if (task.getDetails() == null) task.setDetails("");
            if (task.getFiles() == null) {
                files = new ArrayList<>();
            } else {
                fileupload(task.getFiles(), gridFSFilesBucket, false);
                files = getFileIds();
            }
            if (task.getQuestions() == null) task.setQuestions(new ArrayList<>());
            if (task.getHarvester() == null) task.setHarvester("");
            if (task.getReviewer() == null) task.setReviewer("");
            MongoCollection<Document> taskCollection = appDB.getCollection("Tasks");
            Document taskRecord = new Document("_id", task.getTaskId());
            taskRecord.append("title", task.getTitle())
                    .append("subtitle", task.getSubTitle())
                    .append("questionno", task.getNumOfQuestions())
                    .append("costperquestion", task.getPricePerQuestion())
                    .append("details", task.getDetails())
                    .append("currency", task.getCurrency().toString())
                    .append("status", task.getStatus().toString())
                    .append("harvester", task.getHarvester())
                    .append("reviewer", task.getReviewer())
                    .append("questions", task.getQuestions())
                    .append("files", files);
            taskCollection.insertOne(taskRecord);
            Utility.showAlert(Alert.AlertType.INFORMATION, "Task has been successfully created");
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    private static void fileupload(List<Files> files, GridFSBucket gridFSBucket, boolean isQuestionImage) {
        try {
            if (files != null) {
                ArrayList<Files> ids = new ArrayList<>();
                for (Files file : files) {
                    if (file != null) {
                        InputStream streamToUploadFrom = new FileInputStream(file.getFilePath());
                        ObjectId bucketId = gridFSBucket.uploadFromStream(file.getFilePath(), streamToUploadFrom);
                        file.setBucketId(bucketId);
                    }
                    ids.add(file);
                }
                if (isQuestionImage) {
                    setQuestionFileIds(ids);
                } else
                    setFileIds(ids);
            }
        } catch (FileNotFoundException e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. File could not be found.");
            e.printStackTrace();
        }
    }

    private static ArrayList<Files> getFileIds() {
        return fileIds;
    }

    private static void setFileIds(ArrayList<Files> fileIds) {
        MongoDBController.fileIds = fileIds;
    }

    private static ArrayList<Files> getQuestionFileIds() {
        return questionFileIds;
    }

    private static void setQuestionFileIds(ArrayList<Files> fileIds) {
        MongoDBController.questionFileIds = fileIds;
    }

    static ObservableList<Task> getTasks() {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
            MongoCollection<Document> userCollection = getDatabase().getCollection("Tasks");
            List<Document> task1 = userCollection.find().into(new ArrayList<>());
            tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    static void updateTask(Task task) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
           MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            GridFSBucket gridFSFilesBucket = GridFSBuckets.create(getDatabase(), "files");
            if (task.getDetails() == null) task.setDetails("");
            for (Question q : task.getQuestions()) {
                setQuestionFileIds(new ArrayList<>());
                List<Files> fileImages = new ArrayList<>();
                ArrayList<String> questionFiles = new ArrayList<>();
                if (q.getPathToImage() != null) {
                    fileImages.add(new Files(q.getImageDescription(), q.getPathToImage()));
                }
                if (!q.getOptions().isEmpty() || q.getOptions() != null) {
                    for (QuestionOption qo : q.getOptions()) {
                        if (qo != null && qo.getIsOptionAnImage()) {
                            fileImages.add(new Files(qo.getOptionText(), qo.getPathToOptionImage()));
                        }
                    }
                }
                if (!fileImages.isEmpty()) {
                    fileupload(fileImages, gridFSFilesBucket, true);
                    for (Files files1 : getQuestionFileIds()) {
                        if (files1 != null) {
                            questionFiles.add(files1.getBucketId().toString());
                            q.setBucketIds(questionFiles);
                        }
                    }
                }
            }
            if (task.getQuestions() == null) task.setQuestions(new ArrayList<>());
            if (task.getQuestionIDs() == null) task.setQuestionIDs(new HashMap<>());
            Bson filter = eq("_id", task.getTaskId());
            Bson update1 = set("title", task.getTitle());
            Bson update2 = set("subtitle", task.getSubTitle());
            Bson update3 = set("questionno", task.getNumOfQuestions());
            Bson update4 = set("costperquestion", task.getPricePerQuestion());
            Bson update5 = set("details", task.getDetails());
            Bson update6 = set("currency", task.getCurrency().toString());
            Bson update7 = set("status", task.getStatus().toString());
            Bson update8 = set("harvester", task.getHarvester());
            Bson update9 = set("reviewer", task.getReviewer());
            Bson update10 = set("questions", task.getQuestions());
            Bson update11 = set("questionids", task.getQuestionIDs());
            Bson updateOperation = combine(update1, update2, update3, update4, update5, update6, update7, update8, update9
                    , update10, update11);
            tasksCollection.updateOne(filter, updateOperation);
            Utility.showAlert(Alert.AlertType.INFORMATION, "Task has been successfully updated");
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static ObservableList<Task> getCompletedTasks(Boolean completed) {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            User user = HomeScreenController.user;
            if (completed) {
                List<Document> task1 = tasksCollection.find(and(eq("status", "Closed"), or(eq("harvester", user.getUserName()), eq("reviewer", user.getUserName())))).into(new ArrayList<>());
                List<Document> task2 = tasksCollection.find(and(eq("status", "Completed"), or(eq("harvester", user.getUserName()), eq("reviewer", user.getUserName())))).into(new ArrayList<>());
                tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
                tasks.addAll(FXCollections.observableArrayList(Utility.getTaskArray(task2)));
            } else {
                List<Document> task1 = tasksCollection.find(eq("status", "Closed")).into(new ArrayList<>());
                List<Document> task2 = tasksCollection.find(eq("status", "Completed")).into(new ArrayList<>());
                tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
                tasks.addAll(FXCollections.observableArrayList(Utility.getTaskArray(task2)));
            }
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    static void closeTask(Task task) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            Bson filter = eq("_id", task.getTaskId());
            Bson updateOperation = set("status", "Closed");
            tasksCollection.updateOne(filter, updateOperation);
            createNewPayment(task);
            Utility.showAlert(Alert.AlertType.INFORMATION, "Task has been successfully closed and payment has been generated");
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    private static void createNewPayment(Task task) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> paymentCollection = getDatabase().getCollection("Payments");
            MongoCollection<Document> userCollection = getDatabase().getCollection("Users");
            Document paymentRecord = new Document("_id", String.valueOf(new ObjectId()));
            paymentRecord.append("taskid", task.getTaskId())
                    .append("harvester", task.getHarvester())
                    .append("harvestermethod", Objects.requireNonNull(userCollection.find(eq("username", task.getHarvester())).first()).get("paymentmethod"))
                    .append("reviewer", task.getReviewer())
                    .append("reviewermethod", Objects.requireNonNull(userCollection.find(eq("username", task.getReviewer())).first()).get("paymentmethod"))
                    .append("amount", task.getPricePerQuestion() * task.getNumOfQuestions())
                    .append("currency", task.getCurrency().toString())
                    .append("status", PaymentStatus.Pending.toString());
            paymentCollection.insertOne(paymentRecord);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static ObservableList<Task> getHarvestTasks() {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            List<Document> task1 = tasksCollection.find(eq("status", "Created")).into(new ArrayList<>());
            tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    static ObservableList<Task> getReviewTasks() {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
           MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            List<Document> task1 = tasksCollection.find(eq("status", "Harvested")).into(new ArrayList<>());
            tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    static void requestTask(Task task, User user) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            Bson filter = eq("_id", task.getTaskId());
            String userRole;
            String status;
            if (user.getRole().toString().equals("Harvester")) {
                userRole = "harvester";
                status = TaskStatus.HarvestRequested.toString();
            } else {
                userRole = "reviewer";
                status = TaskStatus.ReviewRequested.toString();
            }
            Bson update1 = set("status", status);
            Bson update2 = set(userRole, user.getUserName());
            Bson updateOperation = combine(update1, update2);
            tasksCollection.updateOne(filter, updateOperation);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }

    }

    static ObservableList<Task> getUserTasks(User user) {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            List<Document> task1;
            if (user.getRole().equals(Role.Harvester))
                task1 = tasksCollection.find(and(eq("harvester", user.getUserName()), in("status", FXCollections.observableArrayList("HarvestRequested", "Harvesting", "Harvested", "ReviewRequested", "Reviewing", "Reviewed")))).into(new ArrayList<>());
            else
                task1 = tasksCollection.find(and(eq("reviewer", user.getUserName()), in("status", FXCollections.observableArrayList("HarvestRequested", "Harvesting", "Harvested", "ReviewRequested", "Reviewing", "Reviewed")))).into(new ArrayList<>());
            tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    static void setStatus(Task task, TaskStatus status, String harvester, String reviewer) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
           MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            Bson filter = eq("_id", task.getTaskId());
            Bson update1 = set("status", status.toString());
            Bson update2 = set("harvester", harvester);
            Bson update3 = set("reviewer", reviewer);
            Bson updateOperation = combine(update1, update2, update3);
            tasksCollection.updateOne(filter, updateOperation);
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static ObservableList<Task> getRequests() {
        ObservableList<Task> tasks = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> tasksCollection = getDatabase().getCollection("Tasks");
            List<Document> task1;
            task1 = tasksCollection.find(in("status", FXCollections.observableArrayList("HarvestRequested", "ReviewRequested"))).into(new ArrayList<>());
            tasks = FXCollections.observableArrayList(Utility.getTaskArray(task1));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return tasks;
    }

    public static void fetchFileData(ObjectId fileId) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
           GridFSBucket gridFSFilesBucket = GridFSBuckets.create(getDatabase(), "files");
            GridFSFile file = gridFSFilesBucket.find(eq("_id", fileId)).first();
            GridFSDownloadOptions downloadOptions = new GridFSDownloadOptions().revision(0);
            FileOutputStream streamToDownloadTo;
            if (file != null) {
                streamToDownloadTo = new FileOutputStream( path + "questionImages\\" + file.getFilename().substring(file.getFilename().lastIndexOf("\\") + 1));
            gridFSFilesBucket.downloadToStream(file.getFilename(), streamToDownloadTo, downloadOptions);
            streamToDownloadTo.close();
        }
    } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void downloadFiles(Files file) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            GridFSBucket gridFSFilesBucket = GridFSBuckets.create(getDatabase(), "files");
            GridFSDownloadOptions downloadOptions = new GridFSDownloadOptions().revision(0);
            FileOutputStream streamToDownloadTo = new FileOutputStream(path + "downloadFiles\\" + file.getFileName());
            gridFSFilesBucket.downloadToStream(file.getFilePath(), streamToDownloadTo, downloadOptions);
            streamToDownloadTo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static ObservableList<Payment> getPayments(User user) {
        ObservableList<Payment> payments = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
           MongoCollection<Document> paymentsCollection = getDatabase().getCollection("Payments");
            if (user.getRole().equals(Role.Harvester)) {
                List<Document> payment1 = paymentsCollection.find(and(in("status", FXCollections.observableArrayList("Pending", "PartiallyCompleted")), eq("harvester", user.getUserName()))).into(new ArrayList<>());
                List<Document> payment2 = paymentsCollection.find(and(eq("status", "Completed"), eq("harvester", user.getUserName()))).into(new ArrayList<>());
                payments = FXCollections.observableArrayList(Utility.getPaymentsArray(payment1));
                payments.addAll(FXCollections.observableArrayList(Utility.getPaymentsArray(payment2)));
            } else {
                List<Document> payment1 = paymentsCollection.find(and(in("status", FXCollections.observableArrayList("Pending", "PartiallyCompleted")), eq("reviewer", user.getUserName()))).into(new ArrayList<>());
                List<Document> payment2 = paymentsCollection.find(and(eq("status", "Completed"), eq("reviewer", user.getUserName()))).into(new ArrayList<>());
                payments = FXCollections.observableArrayList(Utility.getPaymentsArray(payment1));
                payments.addAll(FXCollections.observableArrayList(Utility.getPaymentsArray(payment2)));
            }
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return payments;
    }

    static ObservableList<Payment> getPendingPayments() {
        ObservableList<Payment> payments = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
           MongoCollection<Document> paymentsCollection = getDatabase().getCollection("Payments");
            List<Document> payment1 = paymentsCollection.find(in("status", FXCollections.observableArrayList("Pending", "PartiallyCompleted"))).sort(Sorts.descending("status")).into(new ArrayList<>());
            List<Document> payment2 = paymentsCollection.find(eq("status", "Completed")).into(new ArrayList<>());
            payments = FXCollections.observableArrayList(Utility.getPaymentsArray(payment1));
            payments.addAll(FXCollections.observableArrayList(Utility.getPaymentsArray(payment2)));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return payments;
    }

    static void updatePayment(Payment payment) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try {
            MongoCollection<Document> paymentsCollection = getDatabase().getCollection("Payments");
            paymentsCollection.updateOne(eq("_id", payment.getPaymentId()), set("status", String.valueOf(payment.getPaymentState())));
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }

    static String getUserPhoneNumber(String harvesterId) {
        String phoneNumber = "";
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
           MongoCollection<Document> paymentsCollection = getDatabase().getCollection("Users");
            Document user = paymentsCollection.find(new Document("username", harvesterId)).first();
            if (user != null) {
                phoneNumber = user.getString("phonenumber");
            }
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return phoneNumber;
    }

    static User getUser(String harvesterId) {
        User user = null;
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
             MongoCollection<Document> paymentsCollection = getDatabase().getCollection("Users");
            Document user1 = paymentsCollection.find(new Document("username", harvesterId)).first();
            JSONObject currUser;
            if (user1 != null) {
                currUser = new JSONObject(user1.toJson());
                user = new User(currUser.getString("_id"), currUser.getString("firstname"),
                        currUser.getString("lastname"), currUser.getString("username"),
                        Role.valueOf(currUser.getString("role")), PaymentMethod.valueOf(currUser.getString("paymentmethod")));
                user.setBankName(currUser.getString("bankname"));
                user.setBankName(currUser.getString("bankcode"));
                user.setAccountNumber(currUser.getString("accountno"));
                user.setCountry(currUser.getString("country"));
                user.setEmail(currUser.getString("email"));
            }
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return user;
    }

    static User loadUserDetails(User user) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
            MongoCollection<Document> userCollection = Objects.requireNonNull(getDatabase()).getCollection("Users");
            //Document user1 = userCollection.find(new Document("username", "shaliniepeiris")).first();
            Document currUser = userCollection.find(new Document("username", user.getUserName())).first();
            if (currUser != null) {
                JSONObject userOBJ = new JSONObject(currUser.toJson());
                User user1 = new User(userOBJ.getString("_id"), userOBJ.getString("firstname"),
                        userOBJ.getString("lastname"), userOBJ.getString("username"),
                        Role.valueOf(userOBJ.getString("role")), PaymentMethod.valueOf(userOBJ.getString("paymentmethod")));
                user1.setPassword(userOBJ.getString("password"));
                if (!userOBJ.has("accountno")) user1.setAccountNumber(null);
                else user1.setAccountNumber(userOBJ.getString("accountno"));
                if (!userOBJ.has("bankname")) user1.setBankName(null);
                else user1.setBankName(userOBJ.getString("bankname"));
                if (!userOBJ.has("country")) user1.setCountry("");
                else user1.setCountry(userOBJ.getString("country"));
                if (!userOBJ.has("bankcountry")) user1.setBankCountry(null);
                else user1.setBankCountry(userOBJ.getString("bankcountry"));
                if (!userOBJ.has("bankcode")) user1.setBankCode(null);
                else user1.setBankCode(userOBJ.getString("bankcode"));
                if (!userOBJ.has("phonenumber")) user1.setPhoneNumber("");
                else user1.setPhoneNumber(userOBJ.getString("phonenumber"));
                if (!userOBJ.has("email")) user1.setEmail("");
                else user1.setEmail(userOBJ.getString("email"));
                return user1;
            }
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
        return null;
    }

    static void addUser(User user) {
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARNING);
        try  {
            MongoCollection<Document> userCollection = Objects.requireNonNull(getDatabase()).getCollection("Users");
            Document userRecord = new Document("_id", user.getUserId());
            userRecord.append("firstname", user.getFirstName())
                    .append("lastname", user.getLastName())
                    .append("username", user.getUserName())
                    .append("password", user.getPassword())
                    .append("role", String.valueOf(user.getRole()))
                    .append("phonenumber", user.getPhoneNumber())
                    .append("paymentmethod", PaymentMethod.Direct)
                    .append("country", user.getCountry())
                    .append("email", user.getEmail());
            userCollection.insertOne(userRecord);
            Utility.showAlert(Alert.AlertType.INFORMATION, "User has been successfully created. Please log into your account.");
        } catch (Exception e) {
            Utility.showAlert(Alert.AlertType.ERROR, "An error occurred. Please try again.");
            e.printStackTrace();
        }
    }
}
