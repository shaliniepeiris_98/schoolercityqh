package harvest.controller;

import harvest.model.Task;
import harvest.model.TaskStatus;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CompleteTasksController implements Initializable {

    @FXML
    TableView<Task> completedTableView;

    @FXML
    TableColumn<Task, String> completeColumn, completeHarvesterColumn, completeTaskStatusColumn, completeCurrencyColumn,
            completeQuestionCostColumn, completeQuestionNoColumn, completeTaskTitleColumn, completeTaskIdColumn, completeReviewerColumn,
            completeTaskSubtitleColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        tasks = MongoDBController.getCompletedTasks(false);
        completedTableView.setItems(tasks);
        completeTaskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        completeTaskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        completeTaskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        completeQuestionNoColumn.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));
        completeQuestionCostColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerQuestion"));
        completeCurrencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        completeTaskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        completeHarvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        completeReviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button completeButton = new Button("Close Task");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            completeButton.setOnAction(e -> {
                                try {
                                    completeTask(task, e);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            });
                            if (task.getStatus() == TaskStatus.Closed) {
                                completeButton.setDisable(true);
                            }
                            setGraphic(completeButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        completeColumn.setCellFactory(cellFactory);
    }

    private void completeTask(Task task, ActionEvent event) throws IOException {
        MongoDBController.closeTask(task);
        initialize(getClass().getResource("/resources/CompleteTasks.fxml"), null);
    }
}
