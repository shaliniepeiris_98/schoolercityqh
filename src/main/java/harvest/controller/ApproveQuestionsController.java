package harvest.controller;

import harvest.Utility;
import harvest.model.Files;
import harvest.model.Question;
import harvest.model.QuestionOption;
import harvest.model.QuestionStatus;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ApproveQuestionsController implements Initializable {
    private FileChooser fileChooser = new FileChooser();
    static int questionNo;

    @FXML
    Label questionLabel, statusLabel;

    @FXML
    TextField questionTextField;

    @FXML
    TextArea instructionTextArea, changeTextArea;

    @FXML
    VBox questionAnswers, questionVBox;

    @FXML
    Button approveButton, rejectButton, requestButton;

    @FXML
    FlowPane filePane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        questionLabel.setText("Question No. " + (questionNo + 1));
        setQuestionCheck();
    }

    private void setQuestionCheck() {
        Question question = new Question();
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        questionTextField.setText(question.getQuestion());
        instructionTextArea.setText(question.getInstructions());
        changeTextArea.setDisable(false);
        changeTextArea.setText(question.getChangeRequest());
        if (question.getStatus() != null) statusLabel.setText(question.getStatus().toString());
        if (question.getPathToImage() != null) {
            fileAddon(new File(question.getPathToImage()), question);
        }
        if (!question.getOptions().isEmpty()) {
            setAnswerSection(question.getOptions().size());
        }
        Question finalQuestion = question;
        questionTextField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                saveQuestion(finalQuestion);
            }
        });
        instructionTextArea.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                saveQuestion(finalQuestion);
            }
        });
        changeTextArea.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                finalQuestion.setChangeRequest(changeTextArea.getText());
                saveQuestion(finalQuestion);
            }
        });
        if (question.getStatus().equals(QuestionStatus.Approved)) approveButton.setStyle("-fx-border-color:  #000080;");
        if (question.getStatus().equals(QuestionStatus.Rejected)) rejectButton.setStyle("-fx-border-color:  #000080;");
        if (question.getStatus().equals(QuestionStatus.RequestChange))
            requestButton.setStyle("-fx-border-color:  #000080;");
    }

    private void saveQuestion(Question question) {
        if ((questionTextField.getText() != null && !questionTextField.getText().equals("")) ||
                (instructionTextArea.getText() != null && !instructionTextArea.getText().equals("")) ||
                !filePane.getChildren().isEmpty()) {
            question.setQuestion(questionTextField.getText());
            question.setInstructions(instructionTextArea.getText());
            ReviewQuestionsController.task.getQuestionIDs().get("Questions").set(questionNo, question.getQuestionId());
            List<Question> list;
            if (ReviewQuestionsController.task.getQuestions().isEmpty()) {
                list = new ArrayList<>();
                list.add(question);
            } else {
                list = ReviewQuestionsController.task.getQuestions();
                if (!list.contains(question)) {
                    list.add(question);
                }
            }
            ReviewQuestionsController.task.setQuestions(list);
        }
    }

    private void setAnswerSection(int value) {
        questionAnswers.getChildren().clear();
        Question question = new Question();
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        List<QuestionOption> list = question.getOptions();
        if (list.isEmpty()) {
            for (int i = 0; i < value; i++) {
                list.add(null);
            }
        } else {
            if (list.size() <= value) {
                for (int i = list.size(); i < value; i++) {
                    list.add(null);
                }
            } else {
                list = list.subList(0, value);
                question.setOptions(list);
                question.setAnswerId(null);
                if (!ReviewQuestionsController.task.getQuestions().get(questionNo).getQuestionId().equals(question.getQuestionId())) {
                    ReviewQuestionsController.task.getQuestions().add(questionNo, question);
                } else {
                    ReviewQuestionsController.task.getQuestions().set(questionNo, question);
                }
            }
        }
        for (int i = 0; i < value; i++) {
            TextField answer = new TextField();
            answer.setPrefWidth(800);
            answer.setDisable(true);
            HBox hbox = new HBox();
            hbox.getChildren().addAll(answer);
            if (list.get(i) != null) {
                if (list.get(i).getIsOptionAnImage()) {
                    answer.setDisable(true);
                    hbox.getChildren().add(addImage(i));
                } else {
                    answer.setText(list.get(i).getOptionText());
                }
            }
            int finalI = i;
            Question finalQuestion = question;
            answer.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
                if (!newPropertyValue) {
                    saveAnswer(answer, finalI, finalQuestion);
                }
            });
            hbox.setPadding(new Insets(10, 10, 10, 10));
            questionAnswers.getChildren().addAll(hbox);
        }
        selectCorrectAnswer(value, question);
    }

    private void selectCorrectAnswer(int value, Question question) {
        HBox hBox = new HBox();
        ToggleGroup answers = new ToggleGroup();
        for (int i = 0; i < value; i++) {
            ToggleButton toggleButton = new ToggleButton(String.valueOf(i + 1));
            toggleButton.setToggleGroup(answers);
            if (question.getAnswerId() != null && question.getAnswerId().equals(String.valueOf(i))) {
                toggleButton.setSelected(true);
            }
            toggleButton.setDisable(true);
            hBox.getChildren().add(toggleButton);
        }
        questionAnswers.getChildren().add(hBox);
    }


    private Node addImage(int index) {
        Question question = new Question();
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        File file;
        QuestionOption a = new QuestionOption();
        if (question.getOptions().get(index) == null || !question.getOptions().get(index).getIsOptionAnImage()) {
            file = fileChooser.showOpenDialog(new Stage());
            a.setIsOptionAnImage(true);
            String folderPath = System.getProperty("user.dir") + "src\\main\\resources\\questionImages\\";
            a.setPathToOptionImage(folderPath + file.getName());
            a.setOptionText(file.getName());
            question.getOptions().set(index, a);
        } else {
            file = new File(question.getOptions().get(index).getPathToOptionImage());
        }
        Label image = new Label(file.getName());
        ReviewQuestionsController.task.getQuestions().set(questionNo, question);
        return image;
    }

    private void saveAnswer(TextField answer, int index, Question question) {
        if (answer.getText() != null && !answer.getText().equals("")) {
            QuestionOption a = new QuestionOption();
            a.setOptionText(answer.getText());
            List<QuestionOption> answers = question.getOptions();
            answers.set(index, a);
            question.setOptions(answers);
            if (!ReviewQuestionsController.task.getQuestions().get(questionNo).getQuestionId().equals(question.getQuestionId())) {
                ReviewQuestionsController.task.getQuestions().add(questionNo, question);
            } else {
                ReviewQuestionsController.task.getQuestions().set(questionNo, question);
            }
        }
    }

    private void fileAddon(File file, Question question) {
        if (file != null) {
            Button button = new Button(file.getName());
            Files files = new Files(file.getName(), file.getPath());
            button.setOnAction(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to download this file?");
                alert.showAndWait().ifPresent(r -> {
                    if (r == ButtonType.OK) {
                        MongoDBController.downloadFiles(files);
                    }
                });
            });
            ReviewQuestionsController.task.getQuestions().set(questionNo, question);
            filePane.getChildren().add(button);
        }
    }

    public void approveQuestion() {
        approveButton.setStyle("-fx-border-color:  #000080;");
        requestButton.setStyle(null);
        rejectButton.setStyle(null);
        changeTextArea.clear();
        changeTextArea.setDisable(true);
        Question question = null;
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        if (question != null) {
            question.setStatus(QuestionStatus.Approved);
            question.setChangeRequest(changeTextArea.getText());
            ReviewQuestionsController.task.getQuestions().set(questionNo, question);
        }
    }

    public void rejectQuestion() {
        rejectButton.setStyle("-fx-border-color:  #000080;");
        requestButton.setStyle(null);
        approveButton.setStyle(null);
        changeTextArea.clear();
        changeTextArea.setDisable(true);
        Question question = null;
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        if (question != null) {
            question.setStatus(QuestionStatus.Rejected);
            question.setChangeRequest(changeTextArea.getText());
            ReviewQuestionsController.task.getQuestions().set(questionNo, question);
        }
    }

    public void requestChange() {
        requestButton.setStyle("-fx-border-color:  #000080;");
        approveButton.setStyle(null);
        rejectButton.setStyle(null);
        changeTextArea.setDisable(false);
        Utility.showAlert(Alert.AlertType.INFORMATION, "Enter necessary changes in given text field.");
        Question question = null;
        String questionID;
        questionID = ReviewQuestionsController.task.getQuestionIDs().get("Questions").get(questionNo);
        if (!questionID.equals("")) {
            for (Question q : ReviewQuestionsController.task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        if (question != null) {
            question.setStatus(QuestionStatus.RequestChange);
            ReviewQuestionsController.task.getQuestions().set(questionNo, question);
        }
    }
}
