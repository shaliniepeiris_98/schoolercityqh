package harvest.controller;

import harvest.App;
import harvest.Utility;
import harvest.model.Role;
import harvest.model.User;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class SignupController implements Initializable {
    private List<String> countries = new ArrayList<>();

    @FXML
    Button loginFormButton, signupButton, passwordButton, confirmPasswordButton;

    @FXML
    BorderPane signupForm;

    @FXML
    TextField signupUsername, signUpEmail, signUpLastName, signupFirstName, signUpPhoneNumber, passwordTextField, confirmPasswordTextField;

    @FXML
    ChoiceBox<String> countryChoiceBox;

    @FXML
    PasswordField signupPassword, signupConfirmPassword;

    public void signup() {
        checkSignupInput();
    }

    private void checkSignupInput() {
        if (signupFirstName.getText().isEmpty() || signupFirstName.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "First name is invalid. Please enter a correct first name.");
        } else if (signUpLastName.getText().isEmpty() || signUpLastName.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Last name is invalid. Please enter a correct last name.");
        } else if (signupUsername.getText().isEmpty() || signupUsername.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "username is invalid. Please enter a correct username.");
        } else if (signUpEmail.getText().isEmpty() || !emailIsValid(signUpEmail.getText())) {
            Utility.showAlert(Alert.AlertType.WARNING, "Email is invalid. Please enter a correct email.");
        } else if (signUpPhoneNumber.getText().isEmpty() || signUpPhoneNumber.getText().equals(" ") || !numberIsValid(signUpPhoneNumber.getText())) {
            Utility.showAlert(Alert.AlertType.WARNING, "Phone number is invalid. Please enter a correct number starting with \"+[country code]\".");
        } else if (signupPassword.getText().isEmpty() || signupPassword.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Password is invalid. Please enter a correct password.");
        } else if (signupConfirmPassword.getText().isEmpty() || signupConfirmPassword.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Password is invalid. Please enter a correct password.");
        } else if (countryChoiceBox.getValue() == null || countryChoiceBox.getValue().equals("")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Country is invalid. Please enter a correct country.");
        } else if (!signupConfirmPassword.getText().equals(signupPassword.getText())) {
            Utility.showAlert(Alert.AlertType.WARNING, "Passwords do not match. Please enter matching passwords to confirm");
        } else {
            User user = new User(signupFirstName.getText(), signUpLastName.getText(), signupUsername.getText(),
                    signupPassword.getText(), signUpPhoneNumber.getText(), countryChoiceBox.getValue(),
                    Role.Harvester, signUpEmail.getText());
            MongoDBController.addUser(user);
            initialize(getClass().getResource("resources/Signup.fxml"), null);
        }
    }

    private boolean numberIsValid(String text) {
        String phoneRegex = "\\+(9[9876543210]\\d|8[987653210]\\d|7[9876543210]\\d|6[9876543210]\\d|5[9876543210]\\d|" +
                "4[9876543210]\\d|3[9876543210]\\d|2[9876543210]\\d|1[9876432]\\d)\\d{1,14}$";
        Pattern pat = Pattern.compile(phoneRegex);
        if (text == null) return false;
        return pat.matcher(text).matches();
    }

    private boolean emailIsValid(String text) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (text == null) return false;
        return pat.matcher(text).matches();
    }

    public void openLoginForm(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "Login", "Log In Form");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadCountries();
        signUpEmail.clear();
        signupPassword.clear();
        passwordTextField.clear();
        signupPassword.textProperty().addListener((observable, oldValue, newValue) -> {
            passwordTextField.setText(signupPassword.getText());
        });
        signupConfirmPassword.clear();
        confirmPasswordTextField.clear();
        signupConfirmPassword.textProperty().addListener((observable, oldValue, newValue) -> {
            confirmPasswordTextField.setText(signupConfirmPassword.getText());
        });
        passwordTextField.visibleProperty().bind(passwordButton.armedProperty());
        confirmPasswordTextField.visibleProperty().bind(confirmPasswordButton.armedProperty());
        signUpPhoneNumber.setText("+");
        signUpPhoneNumber.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\+\\d*")) {
                signUpPhoneNumber.setText(newValue.replaceAll("[^+\\d]", ""));
            }
        });
        signupFirstName.clear();
        signupFirstName.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[A-Za-z]")) {
                signupFirstName.setText(newValue.replaceAll("[^A-Za-z]", ""));
            }
        });
        signUpLastName.clear();
        signUpLastName.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[A-Za-z]")) {
                signUpLastName.setText(newValue.replaceAll("[^A-Za-z]", ""));
            }
        });
        signupUsername.clear();
        signupUsername.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[^ \\t\\r\\n\\v\\f]")) {
                signupUsername.setText(newValue.replaceAll("[ \\t\\r\\n\\v\\f]", ""));
            }
        });
        countryChoiceBox.setItems(FXCollections.observableList(countries));
    }

    private void loadCountries() {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader("countries.json")) {
            Object obj = jsonParser.parse(reader);

            JSONArray bankList = (JSONArray) obj;
            for (Object bank : bankList) {
                JSONObject newBank = new JSONObject(bank.toString());
                countries.add(newBank.getString("name"));
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }
}
