package harvest.controller;

import harvest.App;
import harvest.Utility;
import harvest.model.Files;
import harvest.model.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.ResourceBundle;

public class EditTaskController implements Initializable {
    static Task task;

    @FXML
    Spinner<Integer> taskQuestionsSpinner;

    @FXML
    Spinner<Double> taskCostSpinner;

    @FXML
    ChoiceBox<String> currencyTypeChoiceBox;

    @FXML
    TextArea detailsTextArea;

    @FXML
    VBox fileVBox;

    @FXML
    Button addFilesButton, saveTaskButton;

    @FXML
    TextField taskTitleTextField, taskSubtitleTextField, taskIdTextField;

    private FileChooser fileChooser = new FileChooser();
    private List<Files> list = new ArrayList<>();
    private ObservableList<String> currencyOptions = FXCollections.observableArrayList("USD", "EUR", "GBP", "INR", "LKR");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currencyTypeChoiceBox.setItems(currencyOptions);
        currencyTypeChoiceBox.setValue(currencyOptions.get(0));
        taskIdTextField.setText(task.getTaskId());
        taskTitleTextField.setText(task.getTitle());
        taskSubtitleTextField.setText(task.getSubTitle());
        taskQuestionsSpinner.getValueFactory().setValue(task.getNumOfQuestions());
        taskCostSpinner.getValueFactory().setValue(task.getPricePerQuestion());
        currencyTypeChoiceBox.setValue(String.valueOf(task.getCurrency()));
        detailsTextArea.setText(task.getDetails());
        list = task.getFiles();
        setfiles();
    }

    public void addFiles() {
        setList(fileChooser.showOpenMultipleDialog(new Stage()));
    }

    private void setList(List<File> list) {
        if (!list.isEmpty()) {
            ArrayList<Files> files = (ArrayList<Files>) task.getFiles();
            for (File file : list) {
                Files files1 = new Files(file.getName(), file.getPath());
                files.add(files1);
            }
            this.list = files;
            setfiles();
        }
    }

    private void setfiles() {
        fileVBox.getChildren().clear();
        Label label = new Label("Click on file name to remove from list");
        fileVBox.getChildren().add(label);
        for (Files file : list) {
            Button button = new Button(file.getFileName());
            button.setOnAction(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to remove this file?");
                alert.showAndWait().ifPresent(r -> {
                    if (r == ButtonType.OK) {
                        list.remove(file);
                        setfiles();
                    }
                });
            });
            fileVBox.getChildren().add(button);
        }
    }

    public void saveTask(ActionEvent event) throws IOException {
        checkFields(event);
    }

    private void checkFields(ActionEvent event) throws IOException {
        if (taskQuestionsSpinner.getEditor().getText().isEmpty() || taskQuestionsSpinner.getEditor().getText().equals("0")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Number of Questions is invalid. Please enter a valid number");
        } else if (taskCostSpinner.getEditor().getText().isEmpty() || taskCostSpinner.getEditor().getText().equals("0")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Cost per Questions is invalid. Please enter a valid cost");
        } else if (currencyTypeChoiceBox.getValue().isEmpty()) {
            Utility.showAlert(Alert.AlertType.WARNING, "Currency is invalid. Please enter a valid currency");
        } else {
            updatetask();
            checkDetails();
            checkFiles();
            MongoDBController.updateTask(task);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            App.changeScene(stage, "HomeScreen", "Home Screen");
        }
    }

    private void updatetask() {
        task.setNumOfQuestions(taskQuestionsSpinner.getValue());
        task.setPricePerQuestion(taskCostSpinner.getValue());
        task.setCurrency(Currency.getInstance(currencyTypeChoiceBox.getValue()));
    }

    private void checkDetails() {
        if (detailsTextArea.getText().isEmpty() || detailsTextArea.getText().equals(" ")) {
            task.setDetails(null);
        }
        task.setDetails(detailsTextArea.getText());
    }

    private void checkFiles() {
        if (list.isEmpty()) {
            return;
        }
        task.setFiles(list);
    }
}
