package harvest.controller;

import harvest.App;
import harvest.model.*;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ViewQuestionsController implements Initializable {
    static Task task;

    @FXML
    FlowPane questionSelectionFlowPane;

    @FXML
    AnchorPane questionAnchorPane;

    @FXML
    Button saveQuestionsButton, viewQuestionsButton;

    public void saveQuestions(ActionEvent event) throws IOException {
        MongoDBController.updateTask(task);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "HomeScreen", "Home Screen");
    }

    public void editQuestions(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        if (HomeScreenController.user.getRole().equals(Role.Harvester)) {
            App.changeScene(stage, "HarvestQuestions", "Harvest Questions");
        } else App.changeScene(stage, "ReviewQuestions", "Review Questions");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        createQuestionButtons();
        createQuestionsInput(0);
    }

    private void createQuestionButtons() {
        ToggleGroup tg = new ToggleGroup();
        for (int i = 0; i < task.getNumOfQuestions(); i++) {
            ToggleButton tb = new ToggleButton(String.valueOf(i + 1));
            int finalI = i;
            tb.setOnAction(e -> createQuestionsInput(finalI));
            tb.setToggleGroup(tg);
            questionSelectionFlowPane.getChildren().add(tb);
        }
    }

    private void createQuestionsInput(int i) {
        questionAnchorPane.getChildren().clear();
        Question question = new Question();
        String questionID;
        questionID = task.getQuestionIDs().get("Questions").get(i);
        if (!questionID.equals("")) {
            for (Question q : task.getQuestions()) {
                if (q.getQuestionId().equals(questionID)) {
                    question = q;
                }
            }
        }
        WebView webView = new WebView();
        AnchorPane.setLeftAnchor(webView, 0.0);
        AnchorPane.setRightAnchor(webView, 0.0);
        AnchorPane.setTopAnchor(webView, 0.0);
        AnchorPane.setBottomAnchor(webView, 0.0);
        WebEngine webEngine = webView.getEngine();
        Question finalQuestion = question;
        webEngine.getLoadWorker().stateProperty().addListener((observableValue, state, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                Document doc = webEngine.getDocument();
                if (finalQuestion.getStatus() != null) {
                    Element questionNo = doc.getElementById("questionStatus");
                    if (!finalQuestion.getStatus().equals(QuestionStatus.RequestChange))
                        questionNo.setTextContent("Question Status: " + finalQuestion.getStatus());
                    else
                        questionNo.setTextContent("Question Status: " + finalQuestion.getStatus() + " - " + finalQuestion.getChangeRequest());
                }
                Element questionNo = doc.getElementById("questionNo");
                questionNo.setTextContent("Question No. " + (i + 1));
                Element questionArea = doc.getElementById("question");
                questionArea.setTextContent(finalQuestion.getQuestion());
                Element instructionArea = doc.getElementById("instructions");
                instructionArea.setTextContent(finalQuestion.getInstructions());
                if (finalQuestion.getImageDescription() != null) {
                    try {
                        Element img = doc.createElement("img");
                        String filename = finalQuestion.getImageDescription();
                        img.setAttribute("src", getClass().getResource("/questionImages/").getPath() + filename);
                        doc.getElementById("image").appendChild(img);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Element p = doc.createElement("p");
                        p.setTextContent("(Image could not be loaded.)");
                        doc.getElementById("image").appendChild(p);
                    }
                }
                if (!finalQuestion.getOptions().isEmpty() || finalQuestion.getOptions().toString().equals("")) {
                    for (QuestionOption questionOption : finalQuestion.getOptions()) {
                        Element answerArea = doc.createElement("div");
                        answerArea.setTextContent(finalQuestion.getOptions().indexOf(questionOption) + 1 + ")   " +
                                questionOption.getOptionText());
                        doc.getElementById("root").appendChild(answerArea);
                        if (questionOption.getIsOptionAnImage()) {
                            setImage(questionOption, doc);
                        }
                    }
                }
                Element answer = doc.createElement("div");
                if (finalQuestion.getAnswerId() != null) {
                    answer.setTextContent("Correct Answer:  " + (Integer.parseInt(finalQuestion.getAnswerId()) + 1));
                } else {
                    answer.setTextContent("Answer:  (undefined)");
                }
                doc.getElementById("root").appendChild(answer);
                Element script = doc.createElement("script");
                script.setTextContent("          MathJax.Hub.Config({\n" +
                        "                extensions: [\"tex2jax.js\"],\n" +
                        "                jax: [\"input/TeX\", \"output/HTML-CSS\"],\n" +
                        "                tex2jax: {\n" +
                        "                      inlineMath: [ ['$','$'], [\"\\\\(\",\"\\\\)\"] ],\n" +
                        "                      displayMath: [ ['$$','$$'], [\"\\\\[\",\"\\\\]\"] ],\n" +
                        "                      processEscapes: true\n" +
                        "                },\n" +
                        "                \"HTML-CSS\": { availableFonts: [\"TeX\"] }\n" +
                        "          });");
                script.setAttribute("type", "text/x-mathjax-config");
                Element script1 = doc.createElement("script");
                script1.setAttribute("type", "text/javascript");
                script1.setAttribute("src", "mathjax.js?config=TeX-AMS-MML_HTMLorMML");
                doc.getElementById("head").appendChild(script);
                doc.getElementById("head").appendChild(script1);
            }
        });
        webEngine.load(getClass().getResource("/html/question.html").toString());
        questionAnchorPane.getChildren().add(webView);

    }

    private void setImage(QuestionOption questionOption, Document doc) {
        Element imageArea = doc.createElement("div");
        if (questionOption.getOptionText() != null) {
            try {
                Element image = doc.createElement("img");
                image.setAttribute("src", getClass().getResource("/questionImages/").getPath() + questionOption.getOptionText());
                imageArea.appendChild(image);
                doc.getElementById("root").appendChild(imageArea);
            } catch (Exception e) {
                e.printStackTrace();
                Element p = doc.createElement("p");
                p.setTextContent("(Image could not be loaded.)");
                imageArea.appendChild(p);
                doc.getElementById("root").appendChild(imageArea);
            }
        }
    }
}
