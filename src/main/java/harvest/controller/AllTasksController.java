package harvest.controller;

import harvest.App;
import harvest.model.Task;
import harvest.model.TaskStatus;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AllTasksController implements Initializable {
    @FXML
    TableView<Task> tableView;

    @FXML
    TableColumn<Task, String> reviewerColumn, harvesterColumn, taskStatusColumn, currencyColumn, questionCostColumn, questionNoColumn,
            taskTitleColumn, taskSubtitleColumn, taskIdColumn, editColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        tasks = MongoDBController.getTasks();
        tableView.setItems(tasks);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        taskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        questionNoColumn.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));
        questionCostColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerQuestion"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        taskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button editButton = new Button("Edit");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            editButton.setOnAction(e -> {
                                try {
                                    editTask(task, e);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            });
                            if (task.getStatus() != TaskStatus.Created) {
                                editButton.setDisable(true);
                            }
                            setGraphic(editButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        editColumn.setCellFactory(cellFactory);
    }

    private void editTask(Task task, ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        EditTaskController.task = task;
        App.changeScene(stage, "EditTask", "Edit Task");
    }
}
