package harvest.controller;

import harvest.Utility;
import harvest.model.Files;
import harvest.model.Task;
import harvest.model.TaskStatus;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.ResourceBundle;

public class CreateTaskController implements Initializable {
    @FXML
    Spinner<Integer> taskQuestionsSpinner;

    @FXML
    Spinner<Double> taskCostSpinner;

    @FXML
    ChoiceBox<String> currencyTypeChoiceBox;

    @FXML
    TextArea detailsTextArea;

    @FXML
    VBox fileVBox;

    @FXML
    Button addFilesButton, saveTaskButton;

    @FXML
    TextField taskTitleTextField, taskSubtitleTextField;

    private FileChooser fileChooser = new FileChooser();
    private Task task;
    private List<Files> list = new ArrayList<>();
    private ObservableList<String> currencyOptions = FXCollections.observableArrayList("USD", "EUR", "GBP", "INR", "LKR");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currencyTypeChoiceBox.setValue(currencyOptions.get(0));
        currencyTypeChoiceBox.setItems(currencyOptions);
    }

    public void addFiles() {
        setList(fileChooser.showOpenMultipleDialog(new Stage()));
    }

    private void setList(List<File> list) {
        ArrayList<Files> files = new ArrayList<>();
        for (File file : list) {
            Files files1 = new Files(file.getName(), file.getPath());
            files.add(files1);
        }
        this.list = files;
        setfiles();
    }

    private void setfiles() {
        fileVBox.getChildren().clear();
        Label label = new Label("Click on file name to remove from list");
        fileVBox.getChildren().add(label);
        for (Files file : list) {
            Button button = new Button(file.getFileName());
            button.setOnAction(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to remove this file?");
                alert.showAndWait().ifPresent(r -> {
                    if (r == ButtonType.OK) {
                        list.remove(file);
                        setfiles();
                    }
                });
            });
            fileVBox.getChildren().add(button);
        }
    }

    public void createTask() {
        checkFields();
    }

    private void checkFields() {
        if (taskTitleTextField.getText().isEmpty() || taskTitleTextField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Task title is invalid. Please enter a task title");
        } else if (taskSubtitleTextField.getText().isEmpty() || taskSubtitleTextField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Task subtitle is invalid. Please enter a task subtitle");
        } else if (taskQuestionsSpinner.getEditor().getText().isEmpty() || taskQuestionsSpinner.getEditor().getText().equals("0")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Number of Questions is invalid. Please enter a valid number");
        } else if (taskCostSpinner.getEditor().getText().isEmpty() || taskCostSpinner.getEditor().getText().equals("0")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Cost per Questions is invalid. Please enter a valid cost");
        } else if (currencyTypeChoiceBox.getValue().isEmpty()) {
            Utility.showAlert(Alert.AlertType.WARNING, "Currency is invalid. Please enter a valid currency");
        } else {
            task = new Task(taskTitleTextField.getText(), taskSubtitleTextField.getText(), taskQuestionsSpinner.getValue(),
                    taskCostSpinner.getValue(), Currency.getInstance(currencyTypeChoiceBox.getValue()), TaskStatus.Created);
            checkDetails();
            checkFiles();
            MongoDBController.saveTask(task);
            clearform();
        }
    }

    private void clearform() {
        taskTitleTextField.clear();
        taskSubtitleTextField.clear();
        taskQuestionsSpinner.getValueFactory().setValue(1);
        taskCostSpinner.getValueFactory().setValue(01.00);
        currencyTypeChoiceBox.setValue(currencyOptions.get(0));
        detailsTextArea.clear();
        fileVBox.getChildren().clear();
        list.clear();
    }

    private void checkDetails() {
        if (detailsTextArea.getText().isEmpty() || detailsTextArea.getText().equals(" ")) {
            return;
        }
        task.setDetails(detailsTextArea.getText());
    }

    private void checkFiles() {
        if (list.isEmpty()) {
            return;
        }
        task.setFiles(list);
    }


}
