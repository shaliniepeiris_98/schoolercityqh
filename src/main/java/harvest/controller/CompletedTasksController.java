package harvest.controller;

import harvest.model.Task;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;

public class CompletedTasksController implements Initializable {
    @FXML
    TableView<Task> tableView;

    @FXML
    TableColumn<Task, String> reviewerColumn, harvesterColumn, taskStatusColumn, currencyColumn, questionCostColumn, questionNoColumn,
            taskTitleColumn, taskIdColumn, taskSubtitleColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        tasks = MongoDBController.getCompletedTasks(true);
        tableView.setItems(tasks);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        taskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        questionNoColumn.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));
        questionCostColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerQuestion"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        taskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
    }

}
