package harvest.controller;

import harvest.model.Role;
import harvest.model.Task;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.net.URL;
import java.util.ResourceBundle;

public class AvailableTasksController implements Initializable {
    @FXML
    TableView<Task> tableView;

    @FXML
    TableColumn<Task, String> harvesterColumn, taskStatusColumn, currencyColumn, questionCostColumn, questionNoColumn, taskTitleColumn,
            taskIdColumn, reviewerColumn, acceptColumn, taskSubtitleColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        if (HomeScreenController.user.getRole().equals(Role.Harvester))
            tasks = MongoDBController.getHarvestTasks();
        else tasks = MongoDBController.getReviewTasks();
        tableView.setItems(tasks);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        taskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        questionNoColumn.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));
        questionCostColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerQuestion"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        taskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
        addButtonToTable();
    }

    private void addButtonToTable() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button acceptButton = new Button("Accept");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            acceptButton.setOnAction(e -> {
                                AcceptTask(task);
                            });
                            setGraphic(acceptButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        acceptColumn.setCellFactory(cellFactory);
    }

    private void AcceptTask(Task task) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to accept/request this task?");
            alert.showAndWait().ifPresent(r -> {
                if (r == ButtonType.OK) {
                    MongoDBController.requestTask(task, HomeScreenController.user);
                    initialize(getClass().getResource("/resources/AvailableTasks.fxml"), null);
                }
            });
    }
}
