package harvest.controller;

import harvest.App;
import harvest.model.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UserOngoingTasksController implements Initializable {
    @FXML
    TableView<Task> tableView;

    @FXML
    TableColumn<Task, String> harvesterColumn, taskStatusColumn, currencyColumn, questionCostColumn, questionNoColumn, taskTitleColumn,
            taskIdColumn, reviewerColumn, openColumn, taskSubtitleColumn, submitColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Task> tasks;
        tasks = MongoDBController.getUserTasks(HomeScreenController.user);
        tableView.setItems(tasks);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        taskSubtitleColumn.setCellValueFactory(new PropertyValueFactory<>("subTitle"));
        questionNoColumn.setCellValueFactory(new PropertyValueFactory<>("numOfQuestions"));
        questionCostColumn.setCellValueFactory(new PropertyValueFactory<>("pricePerQuestion"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        taskStatusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvester"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewer"));
        addButtonToOpenTask();
        addButtonToSubmitTask();
    }

    private void addButtonToSubmitTask() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button submitButton = new Button("Submit");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            if ((task.getStatus().equals(TaskStatus.Reviewing) && (HomeScreenController.user.getRole().equals(Role.Reviewer) || HomeScreenController.user.getRole().equals(Role.Admin)))
                                    || (task.getStatus().equals(TaskStatus.Harvesting) && (HomeScreenController.user.getRole().equals(Role.Harvester))))
                                submitButton.setOnAction(e -> {
                                    if (checkQuestionsInvalid(task) && !HomeScreenController.user.getRole().equals(Role.Harvester))
                                        approveTask(task);
                                    else submitTask(task);
                                });
                            else submitButton.setDisable(true);
                            setGraphic(submitButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        submitColumn.setCellFactory(cellFactory);
    }

    private boolean checkQuestionsInvalid(Task task) {
        if (!task.getQuestions().isEmpty()) {
            for (Question question : task.getQuestions()) {
                if (!question.getStatus().equals(QuestionStatus.Approved)) {
                    return false;
                }
            }
            return true;
        } else return false;
    }

    private void submitTask(Task task) {
        if (task.getReviewer().equals("") || task.getReviewer() == null) {
            task.setStatus(TaskStatus.Harvested);
        } else if (task.getReviewer().equals(HomeScreenController.user.getUserName())) {
            task.setStatus(TaskStatus.Harvesting);
        } else task.setStatus(TaskStatus.Reviewing);
        MongoDBController.updateTask(task);
        initialize(getClass().getResource("/resources/UserOngoingTasks.fxml"), null);
    }

    private void approveTask(Task task) {
        task.setStatus(TaskStatus.Completed);
        MongoDBController.updateTask(task);
        initialize(getClass().getResource("/resources/UserOngoingTasks.fxml"), null);
    }

    private void addButtonToOpenTask() {
        Callback<TableColumn<Task, String>, TableCell<Task, String>> cellFactory
                = new Callback<TableColumn<Task, String>, TableCell<Task, String>>() {

            @Override
            public TableCell call(final TableColumn<Task, String> param) {

                return new TableCell<Task, String>() {

                    final Button openButton = new Button("Open");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Task task = getTableView().getItems().get(getIndex());
                            if ((task.getStatus().equals(TaskStatus.Reviewing) && (HomeScreenController.user.getRole().equals(Role.Reviewer) || HomeScreenController.user.getRole().equals(Role.Admin)))
                                    || (task.getStatus().equals(TaskStatus.Harvesting) && (HomeScreenController.user.getRole().equals(Role.Harvester))))
                                openButton.setOnAction(e -> {
                                    try {
                                        if (HomeScreenController.user.getRole().equals(Role.Harvester))
                                            openQuestionEditor(task, e);
                                        else openReviewer(task, e);
                                    } catch (IOException ex) {
                                        ex.printStackTrace();
                                    }
                                });
                            else openButton.setDisable(true);
                            setGraphic(openButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        openColumn.setCellFactory(cellFactory);
    }

    private void openQuestionEditor(Task task, ActionEvent event) throws IOException {
        HarvestQuestionsController.task = task;
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "HarvestQuestions", "Harvest Questions Task");
    }

    private void openReviewer(Task task, ActionEvent event) throws IOException {
        ReviewQuestionsController.task = task;
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "ReviewQuestions", "Review Questions Task");
    }
}
