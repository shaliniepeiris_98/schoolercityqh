package harvest.controller;

import harvest.Utility;
import harvest.model.Payment;
import harvest.model.PaymentMethod;
import harvest.model.PaymentStatus;
import harvest.model.User;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class PendingPaymentsController implements Initializable {

    @FXML
    TableColumn<Payment, String> paymentIdColumn, taskIdColumn, harvesterColumn, harvesterMethodColumn, reviewerColumn,
            payReviewerColumn, reviewerMethodColumn, payHarvesterColumn, costColumn, currencyColumn, paymentStatusColumn;

    @FXML
    TableView<Payment> tableView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ObservableList<Payment> payments;
        payments = MongoDBController.getPendingPayments();
        tableView.setItems(payments);
        taskIdColumn.setCellValueFactory(new PropertyValueFactory<>("taskId"));
        paymentIdColumn.setCellValueFactory(new PropertyValueFactory<>("paymentId"));
        harvesterColumn.setCellValueFactory(new PropertyValueFactory<>("harvesterId"));
        harvesterMethodColumn.setCellValueFactory(new PropertyValueFactory<>("harvesterMethod"));
        reviewerColumn.setCellValueFactory(new PropertyValueFactory<>("reviewerId"));
        reviewerMethodColumn.setCellValueFactory(new PropertyValueFactory<>("reviewerMethod"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        currencyColumn.setCellValueFactory(new PropertyValueFactory<>("currency"));
        paymentStatusColumn.setCellValueFactory(new PropertyValueFactory<>("paymentState"));
        setHarvestPay();
        setReviewerPay();
    }

    private void setReviewerPay() {
        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> cellFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {

            @Override
            public TableCell call(final TableColumn<Payment, String> param) {

                return new TableCell<Payment, String>() {

                    final Button reviewPayButton = new Button("Pay Reviewer");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Payment payment = getTableView().getItems().get(getIndex());
                            reviewPayButton.setOnAction(e -> payReviewer(payment));
                            if (!payment.getPaymentState().equals(PaymentStatus.PartiallyCompleted)) {
                                reviewPayButton.setDisable(true);
                            }
                            setGraphic(reviewPayButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        payReviewerColumn.setCellFactory(cellFactory);
    }

    private void payReviewer(Payment payment) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        String text = "Are you sure you are completing this payment? \n User has selected " +
                payment.getReviewerMethod() + " payment method.\n";
        User harvester = MongoDBController.getUser(payment.getReviewerId());
        if (!payment.getReviewerMethod().equals(PaymentMethod.Paystack)) {
            if (payment.getReviewerMethod().equals(PaymentMethod.Direct)) {
                text = text + "Phone Number: " + MongoDBController.getUserPhoneNumber(payment.getReviewerId());
            } else {
                text = text + "Bank Name: " + harvester.getBankName() + "\n";
                text = text + "Account Number: " + harvester.getAccountNumber() + "\n";
                text = text + "Country: " + harvester.getCountry() + "\n";
            }
        } else {
            text = text + "Confirmation will divert to payment portal.";
        }
        alert.setContentText(text);
        alert.showAndWait().ifPresent(r -> {
            if (r == ButtonType.OK) {
                if (!payment.getReviewerMethod().equals(PaymentMethod.Paystack)) {
                    payment.setPaymentState(PaymentStatus.Completed);
                    MongoDBController.updatePayment(payment);
                    initialize(getClass().getResource("resources/PendingPayments.fxml"), null);
                } else {
                    Utility.initializePayment(payment, harvester);
                }
            }
        });
    }

    private void setHarvestPay() {
        Callback<TableColumn<Payment, String>, TableCell<Payment, String>> cellFactory
                = new Callback<TableColumn<Payment, String>, TableCell<Payment, String>>() {

            @Override
            public TableCell call(final TableColumn<Payment, String> param) {

                return new TableCell<Payment, String>() {

                    final Button harvestPayButton = new Button("Pay Harvester");

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                            setText(null);
                        } else {
                            Payment payment = getTableView().getItems().get(getIndex());
                            harvestPayButton.setOnAction(e -> payHarvester(payment));
                            if (!payment.getPaymentState().equals(PaymentStatus.Pending)) {
                                harvestPayButton.setDisable(true);
                            }
                            setGraphic(harvestPayButton);
                            setText(null);
                        }
                    }
                };
            }
        };
        payHarvesterColumn.setCellFactory(cellFactory);
    }

    private void payHarvester(Payment payment) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        String text = "Are you sure you are completing this payment?\nUser has selected " +
                payment.getHarvesterMethod() + " payment method.\n";
        User harvester = MongoDBController.getUser(payment.getHarvesterId());
        if (!payment.getHarvesterMethod().equals(PaymentMethod.Paystack)) {
            if (payment.getHarvesterMethod().equals(PaymentMethod.Direct)) {
                text = text + "Phone Number: " + MongoDBController.getUserPhoneNumber(payment.getHarvesterId());
            } else {
                text = text + "Bank Name: " + harvester.getBankName() + "\n";
                text = text + "Account Number: " + harvester.getAccountNumber() + "\n";
                text = text + "Country: " + harvester.getCountry() + "\n";
            }
        } else {
            text = text + "Confirmation will divert to payment portal.";
        }
        alert.setContentText(text);
        alert.showAndWait().ifPresent(r -> {
            if (r == ButtonType.OK) {
                if (!payment.getHarvesterMethod().equals(PaymentMethod.Paystack)) {
                    payment.setPaymentState(PaymentStatus.PartiallyCompleted);
                    MongoDBController.updatePayment(payment);
                    try {
                        initialize(new URL("file:/C:/Users/speilk/OneDrive%20-%20IFS/Desktop/SPEILKBackup/Personal/SchoolerCityQH/target/classes/resources/PendingPAyments.fxml"), null);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.initializePayment(payment, harvester);
                }
            }
        });
    }
}
