package harvest.controller;

import harvest.App;
import harvest.model.Files;
import harvest.model.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class HarvestQuestionsController implements Initializable {
    static Task task;

    @FXML
    TextArea detailsTextArea;

    @FXML
    FlowPane questionSelectionPane;

    @FXML
    AnchorPane detailsPane;

    @FXML
    HBox fileHBox;

    @FXML
    ScrollPane questionScrollPane, fileScrollPane;

    @FXML
    Button saveQuestionsButton, viewQuestionsButton, viewDetailsButton;

    @FXML
    TextField taskTitleTextField, taskSubtitleTextField, taskIdTextField, currencyTypeTextField, questionCostTextField,
            questionNoTextField;

    private List<Files> list = new ArrayList<>();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currencyTypeTextField.setText(task.getCurrency().toString());
        taskIdTextField.setText(task.getTaskId());
        taskTitleTextField.setText(task.getTitle());
        taskSubtitleTextField.setText(task.getSubTitle());
        questionNoTextField.setText(String.valueOf(task.getNumOfQuestions()));
        questionCostTextField.setText(String.valueOf(task.getPricePerQuestion()));
        detailsTextArea.setText(task.getDetails());
        list = task.getFiles();
        setFiles();
        try {
            createQuestionsInput(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        createQuestionButtons();
    }

    private void createQuestionButtons() {
        ToggleGroup tg = new ToggleGroup();
        for (int i = 0; i < task.getNumOfQuestions(); i++) {
            ToggleButton tb = new ToggleButton(String.valueOf(i + 1));
            int finalI = i;
            tb.setOnAction(e -> {
                try {
                    createQuestionsInput(finalI);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            tb.setToggleGroup(tg);
            questionSelectionPane.getChildren().add(tb);
        }
    }

    private void createQuestionsInput(int i) throws IOException {
        List<String> list;
        if (task.getQuestionIDs().isEmpty()) {
            task.setQuestionIDs(new HashMap<>(1));
            String[] values = new String[task.getNumOfQuestions()];
            Arrays.fill(values, "");
            list = Arrays.asList(values);
            task.getQuestionIDs().put("Questions", list);
        }
        QuestionController.questionNo = i;
        Pane questionPane = FXMLLoader.load(getClass().getResource("/resources/Question.fxml"));
        questionScrollPane.setContent(questionPane);
    }

    private void setFiles() {
        fileHBox.getChildren().clear();
        for (Files file : list) {
            Button button = new Button(file.getFileName());
            button.setOnAction(e -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to download this file?");
                alert.showAndWait().ifPresent(r -> {
                    if (r == ButtonType.OK) {
                        MongoDBController.downloadFiles(file);
                    }
                });
            });
            fileHBox.getChildren().add(button);
        }
    }

    public void saveQuestions(ActionEvent event) throws IOException {
        MongoDBController.updateTask(task);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "HomeScreen", "Home Screen");
    }

    public void viewQuestions(ActionEvent event) throws IOException {
        ViewQuestionsController.task = task;
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "ViewQuestions", "View Questions");
    }

    public void viewDetails(ActionEvent event) {
        detailsPane.setVisible(true);
        detailsPane.setManaged(true);
        viewDetailsButton.setText("Hide Task Details");
        viewDetailsButton.setOnAction(this::hideDetails);
    }

    private void hideDetails(ActionEvent e) {
        detailsPane.setVisible(false);
        detailsPane.setManaged(false);
        viewDetailsButton.setText("View Task Details");
        viewDetailsButton.setOnAction(this::viewDetails);
    }
}
