package harvest.controller;

import harvest.App;
import harvest.Utility;
import harvest.model.Permissions;
import harvest.model.Role;
import harvest.model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class HomeScreenController implements Initializable {
    static User user;

    private ArrayList<String> menu = new ArrayList<>();

    @FXML
    Button profileButton;

    @FXML
    Label userLabel;

    @FXML
    AnchorPane mainPane;

    @FXML
    AnchorPane header;

    @FXML
    BorderPane homePane;

    @FXML
    VBox menuVBox;

    public void showProfile() throws IOException {
        ProfileController.user1 = user;
        mainPane.getChildren().clear();
        AnchorPane profilePane = FXMLLoader.load(getClass().getResource("/resources/Profile.fxml"));
        AnchorPane.setTopAnchor(profilePane, 0.0);
        AnchorPane.setBottomAnchor(profilePane, 0.0);
        AnchorPane.setLeftAnchor(profilePane, 0.0);
        AnchorPane.setRightAnchor(profilePane, 0.0);
        mainPane.getChildren().add(profilePane);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        userLabel.setText(user.getFirstName() + " " + user.getLastName() + " ( " + user.getRole() + " )");
        Permissions permissions = Utility.SetPermissions(user.getRole());
        try {
            setMenu(permissions);
            if (user.getRole().equals(Role.Admin)) {
                getScreen("AllTasks");
            } else getScreen("UserOngoingTasks");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setMenu(Permissions permissions) {
        if (permissions.isAvailableTasks()) menu.add("Available Tasks");
        if (permissions.isAllTasks()) menu.add("All Tasks");
        if (permissions.isCompleteTasks()) menu.add("Complete Tasks");
        if (permissions.isCompletedTasks()) menu.add("Completed Tasks");
        if (permissions.isCreateTask()) menu.add("Create Task");
        if (permissions.isUserPayments()) menu.add("User Payments");
        if (permissions.isPendingPayments()) menu.add("Pending Payments");
        if (permissions.isPromoteUsers()) menu.add("Promote Users");
        if (permissions.isUserOngoingTasks()) menu.add("User Ongoing Tasks");
        if (permissions.isRequests()) menu.add("Requests");
        makeMenu();
    }

    private void makeMenu() {
        for (String screen : menu) {
            Button button = new Button();
            String fxmlName = screen.replace(" ", "");
            button.setId(fxmlName + "Button");
            button.setText(screen);
            button.setPrefSize(199.0, 28.0);
            button.setStyle("-fx-background-color: transparent; -fx-border-color: transparent transparent #000080 transparent;");
            button.setOnAction(e -> {
                try {
                    mainPane.getChildren().clear();
                    getScreen(fxmlName);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            menuVBox.getChildren().add(button);
        }
    }

    private void getScreen(String fxmlName) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("/resources/" + fxmlName + ".fxml"));
        AnchorPane.setTopAnchor(pane, 0.0);
        AnchorPane.setBottomAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        mainPane.getChildren().add(pane);
    }

    public void signOut(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to sign out?");
        alert.showAndWait().ifPresent(r -> {
            if (r == ButtonType.OK) {
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                user = null;
                try {
                    App.changeScene(stage, "Login", "Log In Form");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
