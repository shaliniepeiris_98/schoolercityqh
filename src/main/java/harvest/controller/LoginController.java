package harvest.controller;

import harvest.App;
import harvest.Utility;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {

    @FXML
    Button registrationButton, loginButton;

    @FXML
    BorderPane loginForm;

    @FXML
    TextField loginUsername;

    @FXML
    PasswordField loginPassword;



    public void openSignUpForm(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        App.changeScene(stage, "Signup", "Sign Up Form");
    }

    public void login(ActionEvent event) {
        checkInput(loginUsername, loginPassword, event);
    }


    private void checkInput(TextField textField, PasswordField passwordField, ActionEvent event) {
        if (textField.getText().isEmpty() || textField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Username is invalid. Please enter a correct username");
        } else if (passwordField.getText().isEmpty() || passwordField.getText().equals(" ")) {
            Utility.showAlert(Alert.AlertType.WARNING, "Password is invalid. Please enter a correct password");
        } else {
            MongoDBController.loadUser(textField.getText(), passwordField.getText(), event);
        }
    }
}
