package harvest;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public static void changeScene(Stage stage, String sceneName, String formTitle) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/resources/" + sceneName + ".fxml"));
        Parent root = fxmlLoader.load();
        stage.setTitle(formTitle);
        stage.setScene(new Scene(root));
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        run();
        for(File file: Objects.requireNonNull(new File("C:\\Users\\speilk\\OneDrive - IFS\\Desktop\\SPEILKBackup\\Personal\\" +
                "schoolercityqh\\src\\main\\resources\\questionImages").listFiles())) {
            if (!file.isDirectory()) {
                file.delete();
            }
        }
    }

    private void run() throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/Login.fxml"));
        Parent root = fxmlLoader.load();
        stage.setTitle("User Login");
        stage.setScene(new Scene(root, 1600, 900));
        stage.showAndWait();
    }
}